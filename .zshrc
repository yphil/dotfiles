# My .zshrc - created 01 Jul 2012
# See ~/.dotfiles-commands.sh for common ENV stuff.

#colourful man pages
export LESS_TERMCAP_mb=$'\E[01;31m'       # begin blinking
export LESS_TERMCAP_md=$'\E[01;38;5;74m'  # begin bold
export LESS_TERMCAP_me=$'\E[0m'           # end mode
export LESS_TERMCAP_se=$'\E[0m'           # end standout-mode
export LESS_TERMCAP_so=$'\E[38;5;246m'    # begin standout-mode - info box
export LESS_TERMCAP_ue=$'\E[0m'           # end underline
export LESS_TERMCAP_us=$'\E[04;38;5;146m' # begin underline
export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
# export PATH=$PATH:$HOME/src/depot_tools

setxkbmap fr

# Enable compsys completion.
autoload -U compinit && compinit

# Commodities
autoload -Uz vcs_info
autoload -U colors && colors
autoload -U promptinit && promptinit
autoload colors zsh/terminfo

setopt auto_cd
setopt complete_in_word
setopt emacs
# setopt auto_list
# implied by menu_complete
# setopt auto_menu
setopt menu_complete
setopt extended_glob
setopt no_bare_glob_qual

# HISTORY
# Implied by SHARE_HISTORY
# setopt INC_APPEND_HISTORY
setopt share_history
# timestamp
setopt extended_history
# this is default but hey
setopt hist_save_by_copy
setopt hist_ignore_dups # Do not write events to history that are duplicates of previous events
setopt hist_save_no_dups
setopt hist_find_no_dups # When searching history don't display results already cycled through twice
setopt hist_expire_dups_first
setopt hist_ignore_space # remove command line from history list when first character on the line is a space
setopt bang_hist
setopt append_history # Allow multiple terminal sessions to all append to one zsh command history
setopt inc_append_history # Add comamnds as they are typed, don't wait until shell exit
setopt hist_expire_dups_first # when trimming history, lose oldest duplicates first

# larger than SAVEHIST to accomodate dups
export HISTSIZE=10500
export SAVEHIST=10000
export HISTFILE="$HOME/.zsh_history"
export UNISONLOCALHOSTNAME=$(hostname)

setopt -o sharehistory
setopt list_ambiguous
setopt completealiases
setopt HIST_VERIFY
setopt prompt_subst

bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word

# bindkey "\e[Z" reverse-menu-complete # Shift+Tab in completion menu
bindkey '^[[3;3~' kill-word # alt-del kills word forward
# bindkey "^B" backward-kill-line
bindkey '\^U' backward-kill-line
bindkey -M emacs '^[[3;5~' kill-word

# case-insensitive (uppercase from lowercase) completion
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:git*' formats "%b"

# process completion
zstyle ':completion:*:processes' command 'ps -au$USER'
zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#)*=36=31"

# zstyle
zstyle ':completion:*' menu select=2
zstyle ':completion:*' completer _expand _complete _ignored _approximate
# zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
# zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
# zstyle ':completion:*:descriptions' format '%U%F{yellow}%d%f%u'
# ===== Completion

# compdef pkill=kill
# compdef pkill=killall
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:processes' command 'ps -au$USER'

WORDCHARS="*?_-.[]~&;!#$%^(){}<>"
export WORDCHARS=''

# bindkey '^e' emacs-backward-word

bindkey "^[^[[" forward-word      # Meta-RightArrow
bindkey "^[^[[" backward-word     # Meta-LeftArrow

# bindkey '^[[1;5C' emacs-forward-word
# bindkey '^[[1;5D' emacs-backward-word

bindkey "[C" emacs-forward-word   #control left
bindkey "[D" backward-word        #control right

## ZSH Funcs
insert_sudo () { zle beginning-of-line; zle -U "sudo " }
zle -N insert-sudo insert_sudo
bindkey "^[s" insert-sudo

insert_man () { zle beginning-of-line; zle -U "man " }
zle -N insert-man insert_man
bindkey "^[m" insert-man

insert_help () { zle end-of-line; zle -U " --help" }
zle -N insert-help insert_help
bindkey "^[h" insert-help

insert_ip () { zle end-of-line; zle -U " 192.168." }
zle -N insert-ip insert_ip
bindkey "^[i" insert-ip

# Generic funcs
. ~/.dotfiles-commands.sh

# Init
zdump Africa/Morocco Europe/Paris

if (type "cowsay" > /dev/null && type "fortune" > /dev/null ); then
    cowsay `fortune -a`
else
    [[ -x "${HOME}/.scripts/px-cowsay.pl" ]] && perl ${HOME}/.scripts/px-cowsay.pl dlfp
fi

mcolor(){
    for c; do
        printf '\e[48;5;%dm%03d' ${c} ${c}
    done
    printf '\e[0m \n'
}

if [[ $COLORTERM = gnome-* && $TERM = xterm ]]  && infocmp gnome-256color >/dev/null 2>&1; then TERM=gnome-256color; fi
if tput setaf 1 &> /dev/null; then
    tput sgr0
    if [[ $(tput colors) -ge 256 ]] 2>/dev/null; then
      BASE03=$(tput setaf 234)
      BASE02=$(tput setaf 235)
      BASE01=$(tput setaf 240)
      BASE00=$(tput setaf 241)
      BASE0=$(tput setaf 244)
      BASE1=$(tput setaf 245)
      BASE2=$(tput setaf 254)
      BASE3=$(tput setaf 230)
      YELLOW=$(tput setaf 136)
      ORANGE=$(tput setaf 166)
      RED=$(tput setaf 160)
      MAGENTA=$(tput setaf 125)
      VIOLET=$(tput setaf 61)
      BLUE=$(tput setaf 33)
      CYAN=$(tput setaf 37)
      GREEN=$(tput setaf 64)
    else
      BASE03=$(tput setaf 8)
      BASE02=$(tput setaf 0)
      BASE01=$(tput setaf 10)
      BASE00=$(tput setaf 11)
      BASE0=$(tput setaf 12)
      BASE1=$(tput setaf 14)
      BASE2=$(tput setaf 7)
      BASE3=$(tput setaf 15)
      YELLOW=$(tput setaf 3)
      ORANGE=$(tput setaf 9)
      RED=$(tput setaf 1)
      MAGENTA=$(tput setaf 5)
      VIOLET=$(tput setaf 13)
      BLUE=$(tput setaf 4)
      CYAN=$(tput setaf 6)
      GREEN=$(tput setaf 2)
    fi
    BOLD=$(tput bold)
    RESET=$(tput sgr0)
else
    # Linux console colors. I don't have the energy
    # to figure out the Solarized values
    MAGENTA="\033[1;31m"
    ORANGE="\033[1;33m"
    GREEN="\033[1;32m"
    PURPLE="\033[1;35m"
    WHITE="\033[1;37m"
    BOLD=""
    RESET="\033[m"
fi

if [[ $TERM = "dumb" ]]; then	# in emacs
    # for tramp to not hang, need the following. cf:
    # http://www.emacswiki.org/emacs/TrampMode
        unsetopt zle
        unsetopt prompt_cr
        unsetopt prompt_subst
        unfunction precmd
        unfunction preexec
        PS1='%(?..[%?])%!:%~%# '
else
    precmd() {
        tmux rename-window "$(hostname)"
        vcs_info
        if [[ ! -z  $vcs_info_msg_0_  ]] ; then
            if [[ $vcs_info_msg_0_ =~ "master|dev" ]] ; then
                GIT_COLOR="${BOLD}${RED}"
            else
                GIT_COLOR="${GREEN}"
            fi
            GIT=" - ${GIT_COLOR}$vcs_info_msg_0_${RESET}"
        else
            RIGHT="[${CYAN}$(pwd)${RESET}]"
            GIT=""
        fi

        RIGHT="[${BOLD}${CYAN}$(dirs)${RESET}${GIT}]"
        LEFT="[${BOLD}${CYAN}$(whoami)${RESET}@${BOLD}${MAGENTA}$(echo $HOST)${RESET}-$(date +%H:%M:%S)${RESET}]"
        print "${LEFT} ${RIGHT}"

    }
    PS1="# "
    # RPS1=""
fi
