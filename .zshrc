# # MY .zshrc - created 01 Jul 2012
# # See ~/.dotfiles-commands.sh for common ENV stuff.

autoload -Uz compinit promptinit
compinit
promptinit

export DISABLE_AUTO_TITLE=true
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)
export UNISONLOCALHOSTNAME=$(hostname)

zmodload -i zsh/deltochar
bindkey "^[z" delete-to-char

# Enable inline syntax highlighting
typeset -A ZSH_HIGHLIGHT_STYLES
ZSH_HIGHLIGHT_STYLES[path]='fg=cyan,bold'
ZSH_HIGHLIGHT_STYLES[path_prefix]='fg=cyan'
ZSH_HIGHLIGHT_STYLES[redirection]='fg=cyan,bold'
ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]='fg=magenta,bold'

# Commodities
autoload -Uz vcs_info
# autoload -U colors && colors
autoload -U promptinit && promptinit
# autoload colors zsh/terminfo
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' get-revision true
zstyle ':vcs_info:*' unstagedstr '!'
zstyle ':vcs_info:*' stagedstr '+'
zstyle ':vcs_info:*' formats '%b%u%c'

setopt auto_cd
setopt complete_in_word
# setopt emacs
bindkey -e

zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
# zstyle ':completion:*:*:pp' command 'ps au'
zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#)*=36=31"
zstyle ':completion:*:*:kill:*' menu yes select

# zstyle ':completion:*:*:killall:*:processes' list-colors "=(#b) #([0-9]#)*=36=31"
# zstyle ':completion:*:*:killall:*' menu yes select

# zstyle ':completion:*' completer _expand _complete _ignored _approximate

# Man
# zstyle ':completion:*:manuals' separate-sections true
# zstyle ':completion:*:manuals.(^1*)' insert-sections true

# zstyle ':completion:*' _values 'mp3 files' ~/*.mp3
compdef '_files -g "*.h"' foo
# zstyle -e '*' days 'reply=(Monday Tuesday)'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
# zstyle ':completion:*:processes' command 'ps -au$USER'
zstyle ':completion:*' menu select
# Media Players

# zstyle ':completion:*:*:mpg123:*' file-patterns '*.(mp3|MP3):mp3\ files *(-/):directories'
# zstyle ':completion:*:*:mpg321:*' file-patterns '*.(mp3|MP3):mp3\ files *(-/):directories'
# zstyle ':completion:*:*:mocp:*' file-patterns '*.(wav|WAV|mp3|MP3|ogg|OGG|flac):ogg\ files *(-/):directories'
# zstyle ':completion:*:*:sox:*' file-patterns '*.(ogg|OGG|flac|mp4):ogg\ files *(-/):directories'

zstyle -e ':completion:*:default' list-colors 'reply=("${PREFIX:+=(#bi)($PREFIX:t)(?)*==34=32}:${(s.:.)LS_COLORS}")'

# setopt menu_complete

setopt list_rows_first
setopt extended_glob
setopt no_bare_glob_qual

# HISTORY
export HISTSIZE=10500 # larger than SAVEHIST to accomodate dups
export SAVEHIST=10000
export HISTFILE="$HOME/.zsh_history"
export EXTENDED_HISTFILE_ORG="$HOME/.shell-extended-history.md"

function log_command() {
    # Skip commands prefixed with a space
    [[ $1 == " "* ]] && return

    # Remove trailing whitespace and newlines
    local command_trimmed="${1%"${1##*[![:space:]]}"}"

    # Skip empty commands after trimming
    [[ -z "$command_trimmed" ]] && return

    # Remove newline characters that might have been accidentally added
    command_trimmed="${command_trimmed//$'\n'/}"

    # Escape every | character
    command_trimmed="${command_trimmed//|/\\|}"

    # Format: [timestamp] [PWD with ~ substitution] [command]
    local timestamp=$(date "+%m-%d %H:%M:%S")
    local cwd="${PWD/#$HOME/~}"
    echo "| $timestamp | \`$cwd\` | \`$command_trimmed\` |" >> $EXTENDED_HISTFILE_ORG
}

# Trims EXTENDED_HISTFILE_ORG to the last SAVEHIST lines
trim_extended_history() {
  local max_lines="$SAVEHIST"
  local history_file="$EXTENDED_HISTFILE_ORG"

  # If the file exists and exceeds the max line count, trim it
  if [[ -f "$history_file" ]]; then
    local line_count
    line_count=$(wc -l < "$history_file")

    if (( line_count > max_lines )); then
      # Keep only the last $SAVEHIST lines
      tail -n "$max_lines" "$history_file" > "${history_file}.tmp"
      mv "${history_file}.tmp" "$history_file"
    fi
  fi
}

trim_extended_history

autoload -Uz add-zsh-hook
add-zsh-hook preexec log_command

setopt share_history
setopt extended_history
setopt hist_save_by_copy
setopt hist_ignore_dups
setopt hist_save_no_dups
setopt hist_find_no_dups
setopt hist_expire_dups_first
# setopt hist_ignore_space
setopt nobanghist # Very important: https://unix.stackexchange.com/questions/497328/

setopt append_history # Allow multiple terminal sessions to all append to one zsh command history
setopt inc_append_history # Add comamnds as they are typed, don't wait until shell exit
setopt hist_expire_dups_first # when trimming history, lose oldest duplicates first
setopt HIST_VERIFY

setopt list_ambiguous
setopt completealiases
setopt prompt_subst

bindkey "^U" backward-kill-line
bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word
bindkey '^[[3;3~' kill-word # alt-del kills word forward
bindkey -M emacs '^[[3;5~' kill-word


## ZSH Funcs
insert_sudo () { zle beginning-of-line; zle -U "sudo " }
zle -N insert-sudo insert_sudo
bindkey "^[s" insert-sudo

insert_man () { zle beginning-of-line; zle -U "man " }
zle -N insert-man insert_man
bindkey "^[m" insert-man

insert_help () { zle end-of-line; zle -U " --help" }
zle -N insert-help insert_help
bindkey "^[h" insert-help

# start typing + [Up/Down Arrows] - fuzzy find history
if [[ "${terminfo[kcuu1]}" != "" ]]; then
  autoload -U up-line-or-beginning-search
  zle -N up-line-or-beginning-search
  bindkey "${terminfo[kcuu1]}" up-line-or-beginning-search
fi

if [[ "${terminfo[kcud1]}" != "" ]]; then
  autoload -U down-line-or-beginning-search
  zle -N down-line-or-beginning-search
  bindkey "${terminfo[kcud1]}" down-line-or-beginning-search
fi


WORDCHARS="*?_-.[]~&;!#$%^(){}<>"
export WORDCHARS=''

# bindkey '^e' emacs-backward-word

bindkey "^[^[[" forward-word      # Meta-RightArrow
bindkey "^[^[[" backward-word     # Meta-LeftArrow

# bindkey '^[[1;5C' emacs-forward-word
# bindkey '^[[1;5D' emacs-backward-word

bindkey "[C" emacs-forward-word   #control left
bindkey "[D" backward-word        #control right

# Generic funcs
[[ -a ~/.dotfiles-commands.sh ]] && . ~/.dotfiles-commands.sh

# [[ -a ~/.profile ]] && . ~/.profile

if [[ $COLORTERM = gnome-* && $TERM = xterm ]]  && infocmp gnome-256color >/dev/null 2>&1; then TERM=gnome-256color; fi
if tput setaf 1 &> /dev/null; then
    tput sgr0
    if [[ $(tput colors) -ge 256 ]] 2>/dev/null; then
      BASE03=$(tput setaf 234)
      BASE02=$(tput setaf 235)
      BASE01=$(tput setaf 240)
      BASE00=$(tput setaf 241)
      BASE0=$(tput setaf 244)
      BASE1=$(tput setaf 245)
      BASE2=$(tput setaf 254)
      BASE3=$(tput setaf 230)
      YELLOW=$(tput setaf 136)
      ORANGE=$(tput setaf 208)
      RED=$(tput setaf 160)
      MAGENTA=$(tput setaf 125)
      VIOLET=$(tput setaf 61)
      BLUE=$(tput setaf 33)
      CYAN=$(tput setaf 37)
      GREEN=$(tput setaf 64)
    else
      BASE03=$(tput setaf 8)
      BASE02=$(tput setaf 0)
      BASE01=$(tput setaf 10)
      BASE00=$(tput setaf 11)
      BASE0=$(tput setaf 12)
      BASE1=$(tput setaf 14)
      BASE2=$(tput setaf 7)
      BASE3=$(tput setaf 15)
      YELLOW=$(tput setaf 3)
      ORANGE=$(tput setaf 9)
      RED=$(tput setaf 1)
      MAGENTA=$(tput setaf 5)
      VIOLET=$(tput setaf 13)
      BLUE=$(tput setaf 4)
      CYAN=$(tput setaf 6)
      GREEN=$(tput setaf 2)
    fi
    BOLD=$(tput bold)
    RESET=$(tput sgr0)
else
    # Linux console colors. I don't have the energy
    # to figure out the Solarized values
    MAGENTA="\033[1;31m"
    ORANGE="\033[1;33m"
    GREEN="\033[1;32m"
    PURPLE="\033[1;35m"
    WHITE="\033[1;37m"
    BOLD=""
    RESET="\033[m"
fi

if [[ $TERM = "dumb" ]]; then	# in emacs
    # for tramp to not hang, need the following. cf:
    # http://www.emacswiki.org/emacs/TrampMode
        unsetopt zle
        unsetopt prompt_cr
        unsetopt prompt_subst
        unfunction precmd
        unfunction preexec
        PS1='%(?..[%?])%!:%~%# '
else
    precmd() {

        if [[ $HOST =~ ^m[a-z][a-z]$ ]] ; then
            HOST_COLOR="${BOLD}${ORANGE}"
        else
            HOST_COLOR="${BOLD}${RED}"
        fi


        # tmux rename-window "$(hostname)"
        vcs_info
        if [[ ! -z  ${vcs_info_msg_0_}  ]] ; then
            if [[ ${vcs_info_msg_0_} =~ "master" ]] ; then
                GIT_COLOR="${BOLD}${RED}"
            else
                GIT_COLOR="${GREEN}"
            fi
            GIT=" ${GIT_COLOR}$vcs_info_msg_0_${RESET}"
        else
            RIGHT="[${CYAN}$(pwd)${RESET}]"
            GIT=""
        fi

        RIGHT=" ${HOST_COLOR}$(dirs)${RESET}${GIT}"
        LEFT="${BOLD}${CYAN}$(whoami)${RESET}@${HOST_COLOR}$(echo $HOST)${RESET} ${BOLD}${CYAN}$(date +%H:%M:%S)${RESET}"
        print "${LEFT}${RIGHT}"
    }
    PS1="➜ "
fi

[[ -a ~/src/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]] && . ~/src/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
