(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)

;; Add MELPA and MELPA Stable to use-package sources
(setq use-package-ensure-function #'straight-use-package)
(add-to-list 'straight-recipe-repositories 'melpa)

(server-start)

(setq gc-cons-threshold (* 50 1000 1000))
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs %s loaded in %s with %d garbage collections."
                     (format "%s" emacs-version)
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;; @HOOKS, VARS & MODES

;; Some motherfucking package (or shit) re-sets this var to 'complete' I don't know who
(add-hook 'prog-mode-hook
          (lambda ()
            (setq tab-always-indent t)))

(add-hook
 'js-mode-hook
 (lambda ()
   (add-hook
    'eglot-managed-mode-hook
    (lambda ()
      (flymake-eslint-enable)))
   (eglot-ensure)))

;; (add-to-list 'auto-mode-alist '("\\.json\\'" . js-json-mode))

;; (add-hook 'js2-mode-hook
;;           (lambda () (flycheck-select-checker 'javascript-jshint)))

;; remove trailing spaces
(add-hook 'before-save-hook
          (lambda ()
            (save-excursion
              (goto-char (point-min))
              (while (re-search-forward "[ \t]+$" nil t)
                (replace-match "")))))

(add-hook 'gdscript-mode-hook (lambda () (setq indent-tabs-mode nil)))

(defun my-godot-mode-keybindings ()
  "Define custom key bindings for godot-mode."
  (local-set-key (kbd "q") 'godot-kill-output-window)
  (local-set-key (kbd "s") 'godot-shrink-and-exit-output-window))

(add-hook 'godot-mode-hook 'my-godot-mode-keybindings)

(defun godot-kill-output-window ()
  (interactive)
  (if (string-prefix-p "*godot -" (buffer-name))
      (progn
        (kill-buffer-and-window))))

(defun godot-shrink-and-exit-output-window ()
  (interactive)
  (shrink-window-if-larger-than-buffer)
  (other-window 1))

(defun my-js2-mode-setup ()
  "Custom setup for js2-mode."
  (modify-syntax-entry ?$ "w" (syntax-table)) ; Treat '$' as part of a word.
  (local-set-key (kbd "C-c r") 'eglot-rename))

(add-hook 'js2-mode-hook 'my-js2-mode-setup)

(defun remove-trailing-whitespace ()
  "Remove trailing whitespace before saving."
  (when (not (memq major-mode '(makefile-mode)))
    (delete-trailing-whitespace)))

;; (add-hook 'before-save-hook 'remove-trailing-whitespace)

(defun my-mhtml-mode-hook ()
  (define-key mhtml-mode-map (kbd "²") 'hippie-expand))

(add-hook 'mhtml-mode-hook 'my-mhtml-mode-hook)

(defun untabify-buffer ()
  (interactive)
  (untabify (point-min) (point-max)))

;; (add-hook 'before-save-hook 'untabify-buffer)

(defun my-center-point-vertically ()
  "Recenter when visiting a file in a buffer."
  (when (eq (current-buffer) (window-buffer))
    (recenter (/ (window-height) 2))))

(add-hook 'find-file-hook 'my-center-point-vertically)

;; @PACKS

;; (defun add-to-load-path-recursively (dir)
;;   "Add DIR containing .el files to the `load-path` recursively."
;;   (let ((subdirs (directory-files-recursively dir ".*" t)))
;;     (dolist (subdir subdirs)
;;       (when (and (file-directory-p subdir)
;;                  (directory-files subdir nil "\\.el$"))
;;         (add-to-list 'load-path subdir)))))

;; (add-to-load-path-recursively "~/src/lisp")

;; (load-library "gpt4emacs.el")

(load "~/src/lisp/gpt4emacs/gpt4emacs.el")
(load "~/src/lisp/gpt-intern/gpt-intern.el")

(use-package vdiff
  :ensure t)

(use-package exec-path-from-shell
  :ensure t
  :config
  (exec-path-from-shell-initialize))

(use-package flymake-eslint
    :bind
    (([(f7)] . flymake-goto-next-error)
     ([(shift f7)] . flymake-goto-prev-error)))

(use-package tabgo)

(use-package multiple-cursors)

(use-package gdscript-mode
  :init
  (setq gdscript-godot-executable "/home/px/bin/Godot_v4.2.1-stable_linux.x86_64")
  (setq eglot-events-buffer-size 0)
  :hook (gdscript-mode . eglot-ensure)
  :custom (gdscript-eglot-version 4))

(use-package highlight-indent-guides
  :hook
  ((prog-mode . highlight-indent-guides-mode)))

(use-package projectile
  :ensure t
  :init
  (projectile-mode +1)
  :bind (:map projectile-mode-map
              ("s-p" . projectile-command-map)
              ("C-c p" . projectile-command-map)))

(use-package emmet-mode
  ;; https://docs.emmet.io/cheat-sheet/
  :bind ([(control j)] . emmet-expand-line))

(use-package groovy-mode
  :mode "\\gradle.build")

(use-package emacs
  ;; :ensure nil
  :init
  ;; (setq backup-directory-alist (expand-file-name "backup" user-emacs-directory))
  (setq completion-show-help nil)  ;; Disable help for completions globally
  (setq inhibit-startup-message t)
  (setq tab-always-indent 'complete)
  (setq require-final-newline 't)
  (setq use-short-answers t)
  (setq frame-title-format '("" "%b - Emacs " emacs-version))
  (customize-set-variable 'kill-do-not-save-duplicates t)
  (setq auto-window-vscroll nil)
  ;; (setq default-buffer-file-coding-system 'utf-8)
  (setq enable-recursive-minibuffers t)
  (customize-set-variable 'fast-but-imprecise-scrolling t)
  (customize-set-variable 'scroll-conservatively 101)
  (customize-set-variable 'scroll-margin 0)
  (customize-set-variable 'scroll-preserve-screen-position t)
  (setq-default cursor-type 'bar
                indent-tabs-mode nil
                tab-width 2)
  ;; (set-cursor-color "#00ff00")

  ;; Completely disable backup files
  (setq make-backup-files nil)

  ;; Completely disable auto-save files
  (setq auto-save-default nil)

  ;; Completely disable lock files
  (setq create-lockfiles nil)

  ;; Suppress any remnants
  (setq auto-save-visited-mode nil)

  :hook
  ((prog-mode . display-line-numbers-mode)
   (after-save . executable-make-buffer-file-executable-if-script-p))

  :config
  ;; (set-cursor-color "#00ff00")
  (electric-pair-mode 1)
  (scroll-bar-mode 1)
  (prefer-coding-system 'utf-8)
  (set-default-coding-systems 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-keyboard-coding-system 'utf-8)
  (advice-add 'help-window-display-message :around #'ignore)
  (save-place-mode 1))

;; (defun my-tab-line-tabs-function ()
;;   "Return a list of buffers to display in the tab line.
;; This function filters out buffers whose names contain \"magit\"."
;;   (let ((buffers (buffer-list)))
;;     (seq-remove (lambda (buffer)
;;                   (string-match-p "magit" (buffer-name buffer)))
;;                 buffers)))

;; (setq tab-line-tabs-function #'my-tab-line-tabs-function)

;; (use-package flycheck
;;   :ensure t
;;   :bind
;;   (([(f7)] . flycheck-next-error)
;;    ([(shift f7)] . flycheck-previous-error))
;;   :config
;;   (global-flycheck-mode)
;;   (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)))

;; (([f4] . ivy-switch-buffer)
;;  ([(shift f4)] . counsel-imenu)


(use-package flycheck-package)

(use-package symbol-overlay
  :hook
  (prog-mode . symbol-overlay-mode))

(use-package scss-mode
  :mode "\\.styl")

(use-package web-mode
  :ensure t
  :mode ("\\.html\\'" . web-mode)
  )

(use-package sass-mode
  :ensure t
  :mode (("\\.scss\\'" . sass-mode)
         ("\\.sass\\'" . sass-mode)))

(use-package css-mode
  :mode "\\.css"
  :config)

(use-package markdown-mode
  :ensure t
  :mode ("README\\.md\\'" . gfm-mode) ;; github-flafored markdown
  :magic "\\.md\\'")

(use-package python-mode
  :mode "\\.py\\'"
  :config
  (setq python-shell-interpreter "/usr/bin/python3"))

(use-package yaml-mode
  :mode "\\.yml\\'")

(use-package web-mode
  :mode "\\.ejs\\'")

(use-package easy-kill)

;; (use-package ivy
;;   :ensure t
;;   :diminish ivy-mode
;;   :bind (([(f9) (g)] . gpt-intern-prompt-ivy)
;;          :map ivy-minibuffer-map
;;          ("C-M-w" . ivy-kill-line))
;;   :init

;;   (defun gpt-intern-prompt-ivy (&optional beg end)
;;     "Prompt the user with a query, and send it to Gpt-Intern API.
;; Using Ivy."
;;     (interactive (when (use-region-p) (list (region-beginning) (region-end))))
;;     (setq gpt-intern-query-history (delete-dups (copy-sequence minibuffer-history)))
;;     (ivy-read "Ask: " gpt-intern-query-history
;;               :action (lambda (query)
;;                         (add-to-history 'minibuffer-history query)
;;                         (if (and beg end)
;;                             (gpt-intern-prompt beg end query)
;;                           (gpt-intern-prompt nil nil query)))
;;               :caller 'ivy-gpt-intern-query-history))

;;   (defun gpt4emacs-prompt-ivy (&optional prefix)
;;     "Prompt the user with a query, and send it to Gpt API.
;; Using Ivy."
;;     (interactive "P")
;;     (setq gpt-query-history (delete-dups (copy-sequence minibuffer-history)))
;;     (ivy-read "Ask GPT: " gpt-query-history
;;               :action (lambda (query)
;;                         (add-to-history 'minibuffer-history query)
;;                         (gpt4emacs-prompt prefix query))
;;               :caller 'ivy-gpt-query-history))
;;   :config
;;   (ivy-mode 1)
;;   ;; (setq ivy-use-virtual-buffers t)
;;   (setq ivy-count-format "(%d/%d) "))

(use-package ivy-hydra
  :bind

  ([(f9) (b)] . px/build-current)
  ([(f9) (e)] . eglot)
  ([(f9) (g)] . gpt4emacs-prompt-ivy)
  ([(f9) (k)] . px/kill-other-buffers)
  ([(f9) (l)] . display-line-numbers-mode)
  ([(f9) (r)] . rainbow-mode)
  ([(f9) (w)] . px/websearch))

(use-package ivy-rich)
(add-hook 'after-init-hook 'ivy-rich-mode)

(use-package doom-modeline
  :defer 2
  :config
  :init
  (doom-modeline-mode 1))

(use-package avy
  :defer 2
  :bind
  ([(control S f)] . avy-goto-word-1))

;; (use-package beacon
;;   :defer 5
;;   ;; :custom (cursor-type 'bar)
;;   :config
;;   ;; (global-hl-line-mode t)
;;   (beacon-mode t))

(use-package rainbow-mode
  :defer 5)

(use-package yasnippet-snippets
  :ensure t
  :defer 2
  :hook (prog-mode . yas-minor-mode)
  :bind (:map yas-minor-mode-map
              ("C-c y" . yas-expand))
  :config
  (setq tab-always-indent 'complete)
  (define-key yas-minor-mode-map [tab] nil)
  (define-key yas-minor-mode-map (kbd "TAB") nil)
  ;; (define-key yas-minor-mode-map (kbd "<tab>") #'yas-maybe-expand)
  (yas-define-snippets 'js-mode
                       '(("cl" "console.log(\"$1: \", ${1:object});" "pX console.log()")
                         ("ce" "console.error('$1: (%s)', ${1:object},$2);" "pX console.error()")
                         ("each" "${1:collection}.forEach(function (${2:elem}) {\n $0\n });" "pX each()")
                         ("try" "try { $1\n } catch (err) {\n $2\n }${3: finally {\n $4\n }}" "pX try/catch/finally()")
                         ("JS" "JSON.stringify($1)" "pX JSON Stringification")
                         ("i" "if ($1) {\n$2\n\t} else {\n$3\n\t}\n" "Standard if")
                         ("time" "`(current-time-string)`" "Current Time")))
  (yas-define-snippets 'sh-mode
                       '(("if" "if [ $1 ] ; then\n\t$2\nelse\n\t$3\nfi " "pX if")
                         ("[[" "[[ $1 ]] && $2 || $3" "pX ternary")
                         ("echo" "echo -e \"$1Wopop! \\$\\{$2\}\"" "pX echo"))))

(use-package which-key
  :defer 1
  :config
  (setq which-key-idle-delay 2)
  (which-key-mode))

(use-package swiper)

;; (use-package eglot
;;   :config
;;   (add-to-list 'eglot-server-programs `(js-mode . ("eslint-lsp" "--stdio")))
;;   :hook (prog-mode . eglot-ensure))

(use-package js2-mode
  :defer 1
  :mode "\\.js\\'"
  :init
  ;; (setq js-basic-offset 2)
  (setq-default js2-auto-indent-p t
                js2-cleanup-whitespaceq t
                js2-basic-offset 4
                js2-enter-indents-newline t
                js2-indent-on-enter-key t
                js2-global-externs (list "window" "module" "require" "buster" "sinon" "assert" "refute" "setTimeout" "clearTimeout" "setInterval" "clearInterval" "location" "__dirname" "console" "JSON" "jQuery" "$" "DOMPurify" "Mousetrap" "FileReader" "PTL" "localStorage"))

  :config
  (define-key js-mode-map (kbd "M-.") nil))

(use-package dap-mode
  :ensure t
  :hook
  ((js2-mode . dap-mode)
   (dap-mode . dap-ui-mode)
   (dap-session-created . (lambda (_session) (dap-ui-repl)))
   (dap-terminated . (lambda (_args) (dap-ui-repl-close))))
  :config
  ;; Set up DAP for Node.js debugging
  (require 'dap-node)
  (dap-node-setup)  ;; Automatically installs necessary dap-node-debugger

  ;; Optional: Customize dap-ui for better experience
  (setq dap-auto-configure-features '(sessions locals controls tooltip))

  ;; Add a DAP template for your project
  (dap-register-debug-template "Node::WaplannerParseServer"
                               (list :type "node"
                                     :request "launch"
                                     :name "Node::WaplannerParseServer"
                                     :program "${workspaceFolder}/index.js"
                                     :cwd "${workspaceFolder}"
                                     :runtimeExecutable "node")))


(use-package company
  :config
  (setq company-backends '(company-capf company-dabbrev-code company-files company-keywords))
  (setq company-selection-wrap-around t
        company-idle-delay 0.2
        company-minimum-prefix-length 1)
  (setq company-dabbrev-downcase nil))

  (add-hook 'after-init-hook 'global-company-mode)

(use-package counsel
  :defer 2
  :bind
  (([f4] . ivy-switch-buffer)
   ([(shift f4)] . counsel-imenu)
   ([(meta <)] . counsel-mark-ring)
   ([(control f4)] . kill-buffer)
   ([(control c) (f)] . px/counsel-ag)
   ([(control c) (g)] . counsel-git)
   ([(control d)] . swiper-ag)
   ([(control f)] . swiper)
   ([(control h) (control f)] . counsel-describe-function)
   ([(control h) (control v)] . counsel-describe-variable)
   ([(control o)] . counsel-find-file)
   ;; ([f9] . counsel-rhythmbox)
   :map ivy-minibuffer-map
   ([(control f)] . ivy-yank-word)
   ("M-w" . my-ivy-copy-line)
   :map read-expression-map
   ([(control r)] . counsel-expression-history)
   :map read-expression-map
   ([(control r)] . counsel-expression-history))
  :init
  (setq  ivy-use-virtual-buffers t

         ivy-use-ignore-default t
         ivy-virtual-abbreviate 'abbreviate
         ivy-ignore-buffers '("^\\/" "\\` " "\\`\\*" "\\*scratch\\*")
         ivy-display-style 'fancy
         ivy-count-format "(%d/%d) "
         ivy-initial-inputs-alist nil
         ivy-extra-directories nil)

  (defun my-set-mark-before-counsel-mark-ring ()
    "Set the mark before calling `counsel-mark-ring'."
    (interactive)
    (push-mark))

  :config
  (defun my-ivy-copy-line-action (x)
    "Copy the content of the highlighted line in the Ivy minibuffer to the kill ring."
    (kill-new x))

  ;; (ivy-set-actions
  ;;  'ivy-switch-buffer
  ;;  '(("w" my-ivy-copy-line-action "copy")))

  (defun my-ivy-copy-line ()
    "Copy the content of the highlighted line in the Ivy minibuffer to the kill ring."
    (interactive)
    (ivy-call)
    (ivy-exit-with-action
     (lambda (x)
       (my-ivy-copy-line-action x))))

  (progn
    (define-key
     ivy-switch-buffer-map
     [(control w)]
     (lambda ()
       (interactive)
       (ivy-set-action 'kill-buffer)
       (ivy-done)))

    ;; (advice-add 'counsel-mark-ring :before #'my-set-mark-before-counsel-mark-ring)
    ))

(use-package counsel-projectile
  :bind
  ([(control shift o)] . counsel-projectile-find-file))

(use-package undo-tree
  :defer 1
  :bind
  ("C-z" . undo-tree-undo)
  ("C-S-z" . undo-tree-redo)
  :init
  (global-undo-tree-mode)
  :config
  (setq
   undo-tree-history-directory-alist (quote (("." . "~/.config/emacs/backup/")))
   undo-tree-visualizer-timestamps t
   undo-tree-visualizer-diff t
   undo-tree-auto-save-history t))

(use-package magit
  :defer t
  :bind
  ("C-c C-a" . magit-just-amend)
  ("C-x g" . magit-status)
  :config
  (setq magit-completing-read-function 'ivy-completing-read)

  ;; Auto-close process buffers after quitting
  (setq magit-bury-buffer-function 'magit-mode-quit-window)

  ;; Auto-close diff buffers after applying or staging changes
  (setq magit-diff-refine-hunk 'all)
  (setq magit-diff-buffer-function 'magit-display-buffer-same-window-except-diff-v1))

(use-package auto-package-update
  :config
  (setq auto-package-update-delete-old-versions t)
  (auto-package-update-maybe))

;; @FUNCS

(defun px/diff-against-scratch ()
  "Quickly diff the current buffer against the scratch buffer with vertical diff."
  (interactive)
  (let ((scratch-buffer (get-buffer "*scratch*")))
    (if scratch-buffer
        (let ((ediff-split-window-function #'split-window-horizontally)) ; Force vertical diff
          (ediff-buffers (current-buffer) scratch-buffer))
      (message "Scratch buffer does not exist!"))))


(global-set-key (kbd "C-c d s") 'px/diff-against-scratch) ;; Bind to `C-c d s` or another key combo


(defun px/save-as ()
  "Save the current buffer under a different name, with the current file name pre-filled."
  (interactive)
  (let* ((filename (or buffer-file-name (buffer-name)))
         (full-filename (if buffer-file-name
                            (buffer-file-name)
                          (concat default-directory filename))))
    (write-file (read-file-name "Save file as: " full-filename))))


;; (global-set-key (kbd "C-x C-w") 'counsel-write-file)

(defun px/remove-js-comments (start end)
  "Remove JS comments in the region, excluding URLs."
  (interactive "r")
  (save-excursion
    ;; Only operate within the selected region
    (narrow-to-region start end)

    ;; Remove single-line comments (//...), excluding URLs (http:// or https://)
    (goto-char (point-min))
    (while (re-search-forward "\\(^\\|[^:]\\)\\(//\\).*" nil t)
      (let ((before (match-string 1)))
        ;; Only replace if it's a real comment and not a URL
        (replace-match before)))

    ;; Remove multi-line comments (/*...*/)
    (goto-char (point-min))
    (while (re-search-forward "/\\*\\(.\\|\n\\)*?\\*/" nil t)
      (replace-match ""))

    ;; Widen to the whole buffer after the operation
    (widen)))

(defun px/clean-up-buffer ()
  "Remove multiple empty lines and trailing spaces."
  (interactive)
  (save-excursion
    ;; Remove trailing whitespace
    (goto-char (point-min))
    (while (re-search-forward "[ \t]+$" nil t)
      (replace-match ""))

    ;; Replace multiple blank lines with a single blank line
    (goto-char (point-min))
    (while (re-search-forward "\n\\{2,\\}" nil t)
      (replace-match "\n\n"))))

(defun px/counsel-ag (&optional initial-input initial-directory extra-ag-args ag-prompt)
  "Search for a string in the current dir / project."
  (interactive)
  (let ((extra-ag-args (concat "--ignore TAGS --ignore node_modules " (or extra-ag-args ""))))
    (counsel-ag initial-input initial-directory extra-ag-args ag-prompt)))

(defun px/js-log-statement-two-args (input)
  "Format a JavaScript console.log statement."
  (interactive "sEnter the string: ")
  (let* ((args (split-string input))
         (arg1 (car args))
         (arg2 (cadr args)))
    (insert (format "console.log(\"%s: %%s %s: %%s\", %s, %s);" arg1 arg2 arg1 arg2))))

(defun px/dired-copy-dirname-as-kill ()
  "Copy the current buffer-file-name into the kill ring."
  (interactive)
  (kill-new (buffer-file-name))
  (message "Copied %s to clipboard" (buffer-file-name)))

(defun px/go-to-column (column)
  (interactive "nColumn: ")
  (move-to-column column t))

(global-set-key (kbd "M-g M-c") #'px/go-to-column)

(defun px/create-etags-for-current-buffer ()
  "Create an etags file for the current buffer."
  (interactive)
  (shell-command (concat "etags " (buffer-file-name))))

(defun px/create-etags-for-repo ()
  "Create an etags file for the current repository."
  (interactive)
  (let* ((repo-dir (locate-dominating-file default-directory ".git"))
         (extension (read-string "Enter file extension: ")))
    (when repo-dir
      (shell-command
       (format "cd %s && find . -type f -name \"*.%s\" -exec etags -a {} \\;" repo-dir extension)))))

;; link ok
(defun px/markdown-toc ()
  "Extract level 2 and 3 headings in the current Markdown buffer from point down.
   Headings can be links, then the TOC entry will point to this link.
   The generated and indented TOC will be inserted at point."
  (interactive)
  (let (toc-list markdown-toc)
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward "^\\(##+\\)\\s-+\\(.*\\)" nil t)
        (let* ((level (length (match-string 1)))
               (heading-text (match-string 2))
               heading-id)
          ;; Check if heading is a link
          (if (string-match "\\[\\(.*?\\)\\](\\(.*?\\))" heading-text)
              (setq heading-id (match-string 2 heading-text))  ; Use the URI as the heading-id for links
            ;; If not a link, generate a standard heading-id
            (setq heading-id (concat "#" (downcase (replace-regexp-in-string "[[:space:]]+" "-" heading-text))))
            ;; Remove special characters from the heading-id
            ;; (setq heading-id (replace-regexp-in-string "[,()]" "" heading-id))
            (setq heading-id (replace-regexp-in-string "[^a-zA-Z-#éàùï]" "" heading-id))
            ;; (setq heading-text (replace-regexp-in-string "[,()]" "" heading-text))
            )

          (push (cons level (cons heading-text heading-id)) toc-list))))
    (setq toc-list (reverse toc-list))
    (dolist (item toc-list)
      (message "item: %s" item)
      (let* ((level (car item))
             ;; Extract text from the link using the regular expression
             (link-parts (if (string-match "\\[\\(.*?\\)\\]\\(.*?\\)" (cadr item))
                             (list (match-string 1 (cadr item)) (match-string 2 (cadr item)))
                           (list (cadr item) "")))
             (link-text (car link-parts))
             (indentation (make-string (* 2 (1- (max level 1))) ?\ ))
             ;; Use the extracted text in the output
             (line (format "- [%s](%s)\n" link-text (cddr item))))
        (setq markdown-toc (concat markdown-toc (replace-regexp-in-string "^  " "" (concat indentation line))))))
    (insert markdown-toc)))

(defun px/kill-other-buffers ()
  "Kill all other buffers."
  (interactive)
  (mapc (lambda (buffer)
          (when (and (buffer-file-name buffer) (not (eq buffer (current-buffer))))
            (kill-buffer buffer)))
        (buffer-list)))

(defun px/visit-*Messages* ()
  "Visit buffer `*Messages*."
  (interactive)
  (pop-to-buffer (messages-buffer))
  (goto-char (point-max)))

(defun px/build-current ()
  "Compile project."
  (interactive)
  (message "The current directory is %s" (current-buffer))
  (when (string= "*Async Shell Command*" (format "%s" (current-buffer)))
    (kill-current-buffer))
  (message "Dir: %s" (concat default-directory "build.sh"))
  (async-shell-command (concat default-directory "build.sh")))

(defun px/execute-buffer ()
  "Execute buffer."
  (interactive)
  (let ((compile-command nil))
    (compile buffer-file-name)))

(defun px/tidy-buffer ()
  "Run Tidy HTML parser on current buffer."
  (interactive)
  (if (get-buffer "tidy-errs") (kill-buffer "tidy-errs"))
  (shell-command-on-region (point-min) (point-max)
                           "tidy -f /tmp/tidy-errs -q" t)
  (find-file-other-window "/tmp/tidy-errs")
  (other-window 1)
  (delete-file "/tmp/tidy-errs")
  (message "buffer tidy'ed"))

(defun px/websearch (start end)
  "Websearch from START to END, or word under point."
  (interactive (if (use-region-p)
                   (list (region-beginning) (region-end))
                 (remove nil
                         (save-excursion
                           (list
                            (backward-sexp) (point)
                            (forward-sexp) (point))))))

  (let ((selection (buffer-substring-no-properties start end)))
    (px/flash-region start end)
    (browse-url (concat "https://duckduckgo.com/?q="
                        (url-hexify-string selection)))))

(defun px/move-line-up ()
  "Move up the current line."
  (interactive)
  (transpose-lines 1)
  (forward-line -2)
  (indent-according-to-mode))

(defun px/move-line-down ()
  "Move down the current line."
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1)
  (indent-according-to-mode))

(defun px/toggle-comments ()
  "If region is set, [un]comments it.
Otherwise [un]comments current line."
  (interactive)
  (if (eq mark-active nil)
      (progn
        (beginning-of-line 1)
        (set-mark (point))
        (forward-line)
        (comment-dwim nil))
    (comment-dwim nil))
  (deactivate-mark))

(defun px/insert-end-of-line-char (char)
  "Jump to the end of the current line, insert CHAR, then jump back to point."
  (interactive)
  (let ((st (point)))
    (save-excursion
      (move-end-of-line nil)
      (insert char)
      (goto-char st))))

(defun px/insert-or-enclose-with-signs (left-sign right-sign)
  "Insert LEFT-SIGN and RIGHT-SIGN and place the cursor between them."
  (interactive)
  (if mark-active
      (let ((st (point))
            (ed (mark)))
        (goto-char ed)
        (save-excursion
          (if (> st ed)
              (progn
                (insert left-sign)
                (goto-char st)
                (forward-char 1)
                (insert right-sign))
            (progn
              (insert right-sign)
              (goto-char st)
              (insert left-sign)
              (goto-char (+ 1 ed)))))
        (if (> st ed)
            (goto-char (+ 2 st))
          (goto-char (+ 2 ed))))
    (progn
      (insert left-sign right-sign)
      (backward-char 1))))

(defun px/create-insert-pair-function (key open-sign close-sign)
  "Insert a pair of OPEN-SIGN and CLOSE-SIGN or encloses region with it.
Bind it to KEY, return the function."
  (global-set-key key
                  `(lambda ()
                     (interactive)
                     (px/insert-or-enclose-with-signs ,open-sign ,close-sign))))

(px/create-insert-pair-function [(control \=)] "{" "}")            ; {}
(px/create-insert-pair-function [(control \$)] "$(" ")")           ; $()
(px/create-insert-pair-function [(control \))] "(" ")")            ; ()
(px/create-insert-pair-function [(control \()] "[" "]")            ; []
(px/create-insert-pair-function [(control \<)] "<" ">")            ; <>
(px/create-insert-pair-function [(control \')] "'" "'")            ; ''
(px/create-insert-pair-function [(control \")] "\"" "\"")          ; ""

"test stringtest (string test) stringtest string "

(defun px/select-text-in-delimiters ()
  "Select text between the nearest matching left and right delimiters."
  (interactive)
  (let (start end delim)
    (skip-chars-backward "^<>([{\"'`")
    (setq start (point))
    (setq delim (buffer-substring-no-properties (1- start) start))
    (if (string-match-p "[<>([{\"'`]" delim)
        (progn
          (forward-char 1)
          (skip-chars-forward (concat "^" (cdr (assoc delim
                                                      '(("(" . ")") ("[" . "]") ("{" . "}")
                                                        ("\"" . "\"") ("'" . "'") ("`" . "`"))))))
          (setq end (point))
          (goto-char start)
          (push-mark end t t)))))

(defun px/flash-region (start end &optional timeout)
  "Temporarily highlight region from START to END during TIMEOUT."
  (let ((overlay (make-overlay start end)))
    (overlay-put overlay 'face 'secondary-selection)
    (run-with-timer (or timeout 0.2) nil 'delete-overlay overlay)))

;; (defadvice kill-ring-save (before slick-copy activate compile)
;;   "When called interactively with no active region, COPY a single line instead."
;;   (interactive
;;    (if mark-active (list (region-beginning) (region-end))
;;      (px/flash-region (line-beginning-position) (line-beginning-position 2))
;;      (message "Copied line")
;;      (list (line-beginning-position)
;;            (line-beginning-position 2)))))

(defun modi/multi-pop-to-mark (orig-fun &rest args)
  "Call ORIG-FUN until the cursor moves.
Try the repeated popping up to 10 times."
  (let ((p (point)))
    (dotimes (i 10)
      (when (= p (point))
        (apply orig-fun args)))))

(advice-add 'pop-to-mark-command :around
            #'modi/multi-pop-to-mark)

(defadvice kill-region (before slick-cut activate compile)
  "When called interactively with no active region, KILL a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (message "Killed line")
     (list (line-beginning-position)
           (line-beginning-position 2)))))

(dolist (command '(yank yank-pop))
  (eval `(defadvice ,command (after indent-region activate)
           (and (not current-prefix-arg)
                (member major-mode '(emacs-lisp-mode lisp-mode
                                                     clojure-mode    scheme-mode
                                                     haskell-mode    ruby-mode
                                                     rspec-mode      python-mode
                                                     c-mode          c++-mode
                                                     objc-mode       latex-mode
                                                     plain-tex-mode))
                (let ((mark-even-if-inactive transient-mark-mode))
                  (indent-region (region-beginning) (region-end) nil))))))

;; @KEYS:

(global-unset-key (kbd "M-q"))

;; Mode-specifics

(define-key emacs-lisp-mode-map [(control f5)] 'eval-buffer)
;; (define-key js-mode-map [(meta .)] 'js2-jump-to-definition)

(global-set-key [(kbd "q")] 'godot-kill-output-window)

;; Globals

(global-set-key [remap kill-ring-save] 'easy-kill)

(global-set-key [(insert)] nil)
(global-set-key [(meta s)] 'save-buffer)

(global-set-key [(meta q)] (kbd "C-w")) ; (Cut hijack)

;; (global-set-key (kbd "M-à") 'pop-tag-mark)

(global-set-key [(control meta o)] 'find-file-at-point)

(global-unset-key [(control shift up)])
(global-unset-key [(control shift down)])

(global-set-key [(meta shift up)]  'px/move-line-up)
(global-set-key [(meta shift down)]  'px/move-line-down)

(global-set-key [(meta d)] 'px/toggle-comments)

(global-set-key [(meta j)] (lambda () (interactive) (join-line -1)))

(global-set-key [(control h) (*)] (lambda () (interactive) (switch-to-buffer "*scratch*")))
(global-set-key [(control h) (e)] 'px/visit-*Messages*)
(global-set-key [(control x) (control b)] 'ibuffer)

(global-set-key [(shift f1)] 'delete-other-windows)
(global-set-key [(shift f2)] 'other-window)
(global-set-key [(shift f3)] 'kmacro-end-and-call-macro)
(global-set-key [(control shift f4)] 'ibuffer)
(global-set-key [f5] 'revert-buffer)
(global-set-key [(shift f5)] 'auto-revert-mode)
(global-set-key [f10] 'toggle-truncate-lines)
(global-set-key [(control tab)] 'mode-line-other-buffer)

(global-set-key [(control a)] 'mark-whole-buffer)
(global-set-key [(control q)] 'save-buffers-kill-terminal)
(global-set-key (kbd "S-SPC") 'px/select-text-in-delimiters)

(global-set-key (kbd "M-RET") '(lambda ()
                                 (interactive)
                                 (end-of-line)
                                 (newline)
                                 (indent-for-tab-command)))

(global-set-key (kbd "M-SPC") '(lambda ()
                                 (interactive)
                                 (backward-sexp)
                                 (mark-sexp)))

(global-set-key [(meta down)] (lambda () (interactive) (scroll-up 1)))
(global-set-key [(meta up)] (lambda () (interactive) (scroll-down 1)))

(global-set-key [(meta left)] (lambda () (interactive) (previous-buffer)))
(global-set-key [(meta right)] (lambda () (interactive) (next-buffer)))

;; (global-set-key [(meta up)] '(lambda ()
;;                                       (interactive)
;;                                       (scroll-other-window -1)))

;; (global-set-key [(meta down)] '(lambda ()
;;                                         (interactive)
;;                                         (scroll-other-window 1)))

(global-set-key [(control \;)] '(lambda ()
                                  (interactive)
                                  (px/insert-end-of-line-char ";")))

(global-set-key [(control \,)] '(lambda ()
                                  (interactive)
                                  (px/insert-end-of-line-char ",")))

(global-set-key (kbd "²") 'hippie-expand)

;; @CUSTOM
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(anaconda-mode-eldoc-as-single-line t)
 '(bufler-workspace-mode t)
 '(bufler-workspace-tabs-mode nil)
 '(bufler-workspace-tabs-tab-separator "")
 '(c-basic-offset 2)
 '(comint-buffer-maximum-size 30)
 '(company-quickhelp-color-background "dim gray")
 '(company-quickhelp-color-foreground "dark orange")
 '(counsel-mode t)
 '(css-indent-offset 2)
 '(delete-old-versions t)
 '(delete-selection-mode t)
 '(display-buffer-alist
   '(("\\(?:\\*\\(?:\\(?:Customize Group\\|Occur\\|grep\\|xref\\)\\*\\)\\)" display-buffer-same-window
      (inhibit-same-window))))
 '(doom-modeline-buffer-file-name-style 'auto)
 '(doom-modeline-buffer-modification-icon t)
 '(doom-modeline-buffer-state-icon t)
 '(doom-modeline-check-simple-format nil)
 '(doom-modeline-display-default-persp-name t)
 '(doom-modeline-minor-modes nil)
 '(doom-modeline-project-detection 'project)
 '(eldoc-echo-area-display-truncation-message nil)
 '(eldoc-echo-area-use-multiline-p nil)
 '(global-tab-line-mode t)
 '(gpt-api-key-file "~/.metazdoctor")
 '(gpt-api-url
   "https://api.openai.com/v1/engines/gpt-3.5-turbo/completions")
 '(gpt-model "gpt-3.5-turbo")
 '(help-window-select t)
 '(highlight-indent-guides-method 'column)
 '(inhibit-startup-screen t)
 '(ispell-local-dictionary "fr_FR")
 '(js-indent-level 4)
 '(lsp-headerline-breadcrumb-enable nil)
 '(lsp-ui-doc-show-with-mouse nil)
 '(lsp-ui-imenu-enable nil)
 '(lsp-ui-sideline-enable nil)
 '(markdown-mouse-follow-link nil)
 '(markdown-nested-imenu-heading-index nil)
 '(menu-bar-mode nil)
 '(mode-line-percent-position nil)
 '(org-support-shift-select 'always)
 '(recenter-positions '(0.05 middle 0.9))
 '(recentf-max-saved-items 0)
 '(savehist-mode t)
 '(scroll-conservatively 3)
 '(show-paren-mode t)
 '(swiper-include-line-number-in-search t)
 '(tab-always-indent t)
 '(tab-line-close-button-show nil)
 '(tab-width 2)
 '(tool-bar-mode nil)
 '(version-control nil)
 '(vertico-cycle t)
 '(vertico-group-format nil)
 '(yas-also-auto-indent-first-line t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :extend nil :stipple nil :background "#2c2c2c" :foreground "#f6f3e8" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 113 :width normal :foundry "PfEd" :family "DejaVu Sans Mono"))))
 '(button ((t (:background "#333333" :foreground "#f6f3e8" :underline "cyan"))))
 '(cursor ((t (:background "spring green"))))
 '(font-lock-comment-face ((t (:foreground "seashell4" :slant italic))))
 '(highlight ((t (:underline nil :background "#454545" :foreground "#ffffff"))))
 '(isearch ((t (:background "orange red" :foreground "black"))))
 '(lazy-highlight ((t (:background "dark orange" :foreground "gray10"))))
 '(show-paren-match ((t (:background "slate blue"))))
 '(symbol-overlay-default-face ((t (:underline "deep sky blue"))))
 '(tab-bar ((t (:inherit variable-pitch :background "#181a26" :foreground "white smoke"))))
 '(tab-line ((t (:inherit variable-pitch :background "gray35" :foreground "light gray" :height 1.0))))
 '(tab-line-highlight ((t (:foreground "orange red"))))
 '(tab-line-tab ((t (:inherit tab-line))))
 '(tab-line-tab-current ((t (:inherit tab-line-tab :background "#2c2c2c" :height 1.0))))
 '(tab-line-tab-group ((t (:inherit tab-line))))
 '(tab-line-tab-inactive ((t (:inherit tab-line-tab))))
 '(tab-line-tab-modified ((t (:inherit font-lock-doc-face :foreground "light salmon"))))
 '(tab-line-tab-special ((t nil)))
 '(whitespace-hspace ((t (:foreground "gray25"))))
 '(whitespace-space ((t (:background "grey20" :foreground "gray25"))))
 '(whitespace-tab ((t (:background "grey22" :foreground "dark red")))))

;; Inhibit completion in counsel buffers: C-M-j
