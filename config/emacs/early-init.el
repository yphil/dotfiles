
(setq gc-cons-threshold (* 50 1000 1000))

(when (featurep 'native-compile)
  (setq native-comp-async-report-warnings-errors nil))

(setq package-enable-at-startup nil)

(defvar magic-var "plop")

(setq inhibit-startup-message t)
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(menu-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)
(push '(mouse-color . "white") default-frame-alist)
(set-face-attribute 'default nil
                    :family "DejaVu Sans Mono"
                    :height 113
                    :background "#2c2c2c"
                    :foreground "#f6f3e8")

;; Loads a nice blue theme, avoids the white screen flash on startup.
(load-theme 'wombat t)
