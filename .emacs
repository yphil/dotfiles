(package-initialize)

(let ((default-directory "~/src/elisp-test/"))
  (normal-top-level-add-subdirs-to-load-path)
  (require 'doc-a-mode)

  (require 'mail-bug)
  (setq
   mbug-username "philcm"
   mbug-imap-host-name "mail.riseup.net"
   mbug-smtp-host-name "mail.riseup.net")

  (require 'faustine)
  (require 'faust-mode)
  (add-to-list 'auto-mode-alist
               '("\\.dsp\\'" . faustine-mode))
  (message "Test conf"))

(defvar package-archives)
(defvar message-confirm-send t)
(defvar savehist-save-minibuffer-history t)
(defvar use-package-always-ensure t)
(defvar message-default-charset 'utf-8)
(defvar grep-find-ignored-directories '(".svn" ".git" ".hg" ".bzr"))
(defvar debian-changelog-full-name "Yassin Philip")
(defvar debian-changelog-mailing-address "phil@manyrecords.com")
(defvar ibuffer-saved-filter-groups
  '(("home"
     ("emacs-config" (or (filename . ".emacs.d")
                         (filename . ".emacs")))
     ("Org" (or (mode . org-mode)
                (filename . "OrgMode")))
     ("Faust" (or (mode . faust-mode)
                  (filename . "\\.dsp\\'")))
     ("Help" (or (name . "\*Help\*")
                 (name . "\*Apropos\*")
                 (name . "\*info\*"))))))

(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Vars
(setq-default cursor-type 'bar)
(setq-default indent-tabs-mode nil)
(setq completion-ignore-case  t)

;; ORG
;; (defvar px-agenda-file "~/Dropbox/org/agenda.org")
(setq org-agenda-files '("~/Dropbox/org/"))
(setq org-mobile-directory "~/Dropbox/MobileOrg")
                                        ;(split-window-below)
                                        ;(org-agenda-list)
;; (setq org-mobile-directory "~/Dropbox/MobileOrg")
;; (setq org-mobile-inbox-for-pull "~/Dropbox/org/inbox.org")
;; (setq org-mobile-files '("~/Dropbox/org"))

;; (setq
;;  org-mobile-files px-agenda-file
;;  org-capture-templates
;;  '(("a" "Appointment" entry (file px-agenda-file)
;;     "* %?\n  %^T\n\n")

;;    ("t" "Todo" entry (file+headline px-agenda-file "Unfiled Tasks")
;;     "* TODO %?\n  %i\n  %a")
;;    ("j" "Journal" entry (file+datetree px-agenda-file "Journal")
;;     "* %?\nEntered on %U\n  %i\n  %a")
;;    ("n" "Note" entry (file+datetree px-agenda-file "Unfiled Notes")
;;     "* %?\nEntered on %U\n  %i\n  %a"))

;;  org-todo-keywords '((sequence "TODO" "IN-PROGRESS" "WAITING" "DONE"))

;;  ;; org-agenda-files '(px-agenda-file)
;;  org-log-done t
;;  org-time-stamp-custom-formats '("<%d/%m/%Y %a>" . "<%d %m %Y  %a [%H:%M]>"))

;; (define-key global-map "\C-cl" 'org-store-link)
;; (define-key global-map "\C-ca" 'org-agenda)
;; (define-key global-map "\C-cc" 'org-capture)

(setq px-auto-save-dir "auto-save/")
(setq px-undo-tree-history-dir "undo-tree-history/")
(make-directory (concat user-emacs-directory px-auto-save-dir) t)
(make-directory (concat user-emacs-directory px-undo-tree-history-dir) t)

;; End ORG
(setq inhibit-startup-message t
      require-final-newline t
      scroll-conservatively 200
      scroll-margin 3
      initial-scratch-message ";; Hi! \n\n"
      debug-on-error nil

      frame-title-format
      '("" invocation-name " " emacs-version " %@ "
        (:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name)) "%b")) " [%*]")

      c-basic-offset 2
      js-indent-level 2
      css-indent-offset 2

      auto-save-file-name-transforms
      `((".*" ,(concat user-emacs-directory px-auto-save-dir) t))

      ;; backup-directory-alist `(("." . px-auto-save-dir))
      ;; auto-save-file-name-transforms `(("." . px-auto-save-dir))
      ;; auto-save-file-name-transforms `((".*" ,(concat user-emacs-directory "auto-save/") t))
      ;; auto-save-file-name-transforms `((".*" px-auto-save-dir t))
      backup-by-copying t    ; Don't delink hardlinks
      version-control t      ; Use version numbers on backups
      delete-old-versions t  ; Automatically delete excess backups
      kept-new-versions 20   ; how many of the newest versions to keep
      kept-old-versions 5)

;; (setq backup-directory-alist
;;       `((".*" ,(concat user-emacs-directory "auto-save/") t)))

;; (setq auto-save-file-name-transforms
;;       `((".*" ,(concat user-emacs-directory px-auto-save-dir) t)))


;; (global-linum-mode 1)
(menu-bar-mode -1)
(tool-bar-mode -1)
(show-paren-mode 1)
(savehist-mode 1)
(fset 'yes-or-no-p 'y-or-n-p)
;; (recentf-mode 1)
(delete-selection-mode 1)

(define-key dired-mode-map (kbd "DEL") 'dired-up-directory)

(define-key global-map [(insert)] nil)

(global-set-key [(meta s)] 'save-buffer)

(global-set-key [(meta shift up)]  'move-line-up)
(global-set-key [(meta shift down)]  'move-line-down)

(define-key global-map [(meta j)] (lambda () (interactive) (join-line -1)))
(define-key global-map [(control h) (*)] 'px-scratch)
(define-key global-map [(control x) (control b)] 'ibuffer)

(define-key global-map [f1] 'delete-other-windows)
(define-key global-map [f2] 'other-window)
;; (define-key global-map [f3] 'split-window-vertically)

(define-key global-map [(control shift f4)] 'ibuffer)
(define-key global-map [f5] 'revert-buffer)
(define-key global-map [(shift f5)] 'auto-revert-mode)

(define-key emacs-lisp-mode-map [(control f5)] 'eval-buffer)
(define-key global-map [f7] 'flycheck-next-error)

(define-key global-map [f8] 'kmacro-end-and-call-macro)

(define-key global-map [f10] 'toggle-truncate-lines)
(define-key global-map [(pause)] (kbd "C-x b <return>")) ; Kb macro! (toggle last buffers)
(define-key global-map [f12] (kbd "C-u C-SPC")) ; (Back in mark ring)

(define-key global-map [(control a)] 'mark-whole-buffer)
(define-key global-map [(meta d)] 'px-toggle-comments)
(define-key global-map [(control c) (control g)] 'px-websearch)
(define-key global-map (kbd "S-SPC") 'px-select-text-in-quotes)
(define-key global-map [(control f)] 'isearch-forward-symbol-at-point)
(define-key global-map [(control q)] 'save-buffers-kill-terminal)

(define-key emacs-lisp-mode-map [(control e)] 'eval-defun)
(define-key emacs-lisp-mode-map [(control shift e)] 'eval-last-sexp)
(define-key lisp-interaction-mode-map [(control e)] 'eval-defun)
(define-key lisp-interaction-mode-map [(control shift e)] 'eval-last-sexp)

(define-key global-map (kbd "M-RET") '(lambda ()
                                        (interactive)
                                        (end-of-line)
                                        (newline)
                                        (indent-for-tab-command)))

(define-key global-map (kbd "M-SPC") '(lambda ()
                                        (interactive)
                                        (backward-sexp)
                                        (mark-sexp)))

(define-key global-map [(meta up)] '(lambda ()
                                      (interactive)
                                      (scroll-other-window -1)))

(define-key global-map [(meta down)] '(lambda ()
                                        (interactive)
                                        (scroll-other-window 1)))

(define-key global-map [(control \;)] 'px-insert-end-of-command-sign)
(define-key global-map (kbd "²") 'hippie-expand)

(define-key global-map [(control \+)] 'insert-pair-brace-and-return) ; {} + newline
(define-key global-map [(control \=)] 'insert-pair-brace)            ; {}
(define-key global-map [(control \$)] 'insert-pair-jquery)           ; $()
(define-key global-map [(control \))] 'insert-pair-paren)            ; ()
(define-key global-map [(control \()] 'insert-pair-bracket)          ; []
(define-key global-map [(control \<)] 'insert-pair-single-angle)     ; <>
(define-key global-map [(control \')] 'insert-pair-squote)           ; ''
(define-key global-map [(control \")] 'insert-pair-dbquote)          ; ""

;; Hooks!

;; (add-hook 'after-init-hook '(org-agenda-list))
(add-hook 'before-save-hook 'whitespace-cleanup)

(add-hook 'prog-mode-hook 'linum-mode)

(dolist (hook '(emacs-lisp-mode-hook))
  ;; (add-hook hook 'turn-on-elisp-slime-nav-mode)
  (add-hook hook 'eldoc-mode)
  (add-hook hook 'global-prettify-symbols-mode))

(add-hook 'ibuffer-mode-hook
          '(lambda ()
             (ibuffer-switch-to-saved-filter-groups "home")))

(add-to-list `auto-mode-alist
             '("\\.js\\'" . js-mode)
             '("\\.svg\\'" . xml-mode))

;; (use-package faustine
;;   :ensure t
;;   :config
;;   (add-to-list `auto-mode-alist
;;                '("\\.dsp\\'" . faustine-mode)))

(use-package ensime
  :ensure t)

(use-package jade-mode
  :ensure t
  :mode "\\.jade$")

(use-package handlebars-mode
  :ensure t)

(use-package json-mode
  :ensure t
  :mode "\\.json$"
  :mode "\\.tmpl$"
  :config (setq-default js-indent-level 2))

(use-package emmet-mode
  :ensure t
  :config
  (add-hook 'sgml-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
  (add-hook 'web-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
  (add-hook 'css-mode-hook  'emmet-mode) ;; enable Emmet's css abbreviation.
  ;; (define-key emmet-mode-map (kbd "C-RET") 'emmet-expand-line)

  )

(use-package projectile
  :ensure t
  ;; :init (add-hook 'projectile-grep-finished-hook 'px-delete-n-lines-in-buffer)
  :config
  (projectile-global-mode)
  (setq projectile-mode-line
        '(:eval (format " [%s]" (projectile-project-name))))
  (setq projectile-remember-window-configs t)
  (setq projectile-completion-system 'ivy))

(use-package ag
  :ensure t
  :bind (("C-c s" . projectile-ag))
  :config
  (add-hook 'ag-mode-hook 'toggle-truncate-lines)
  ;; (add-hook 'ag-mode-hook 'px-delete-n-lines-in-buffer)
  (setq ag-highlight-search t)
  (setq ag-reuse-buffers 't))

(use-package mustache-mode
  :ensure t
  :mode "\\.mst$")

(use-package web-mode
  :ensure t
  :mode "\\.html$"
  :mode "\\.php$"
  :mode "\\.mst$"
  ;; :mode "\\.js$"
  :mode "\\.hbs$"
  :config
  (setq web-mode-enable-current-column-highlight t
        web-mode-code-indent-offset 2
        web-mode-markup-indent-offset 2)
  (add-to-list 'web-mode-comment-formats '("javascript" . "//" ))
  (setq web-mode-ac-sources-alist
        '(("css" . (ac-source-words-in-buffer ac-source-css-property))
          ("html" . (ac-source-words-in-buffer ac-source-abbrev))
          ("javascript" . (ac-source-words-in-buffer ac-source-words-in-same-mode-buffers))
          ("jsx" . (ac-source-words-in-buffer ac-source-words-in-same-mode-buffers))))
  (auto-complete-mode 1)
  )

(use-package auto-complete
  :ensure t
  :config
  (global-auto-complete-mode)
  (add-to-list 'ac-modes 'web-mode)
  ;; :init
  (setq-default ac-sources '(ac-source-words-in-buffer
                             ac-source-symbols
                             ac-source-abbrev
                             ac-source-dictionary
                             ac-source-emacs-lisp-features
                             ac-source-features
                             ac-source-filename
                             ac-source-files-in-current-dir
                             ac-source-functions
                             ac-source-symbols
                             ac-source-variables
                             ac-source-words-in-all-buffer
                             ac-source-words-in-buffer
                             ac-source-words-in-same-mode-buffers))

  (use-package beacon
    :ensure t
    :config
    (beacon-mode 1))

  (use-package recentf
    :config
    (setq recentf-max-saved-items 500
          recentf-max-menu-items 15
          ;; disable recentf-cleanup on Emacs start, because it can cause
          ;; problems with remote files
          recentf-auto-cleanup 'never)
    (recentf-mode +1))

  (use-package magit

    :bind
    ("C-c C-a" . magit-just-amend)
    ("C-x g" . magit-status)

    :config
    (setq magit-completing-read-function 'ivy-completing-read))

  (use-package flycheck-cask
    :init (add-hook 'flycheck-mode-hook 'flycheck-cask-setup))

  (use-package markdown-mode
    :ensure t)

  (defun ac-emacs-lisp-mode-setup ()
    (setq ac-sources (append
                      '(ac-source-dictionary
                        ac-source-features
                        ac-source-functions
                        ac-source-variables
                        ac-source-symbols) ac-sources)))
  (add-hook 'emacs-lisp-mode-hook 'ac-emacs-lisp-mode-setup))

(use-package counsel
  :ensure t
  :bind
  (("M-y" . counsel-yank-pop)
   :map ivy-minibuffer-map
   ("M-y" . ivy-next-line))
  :config
  (setq enable-recursive-minibuffers t))

(use-package rainbow-mode
  :ensure t
  :init
  (add-hook 'css-mode-hook #'rainbow-mode))

(use-package ivy-hydra
  :ensure t)

(use-package swiper
  :ensure try
  :bind
  (([f4] . ivy-switch-buffer)
   ([(control c) (f)] . counsel-ag)
   ([(control c) (g)] . counsel-git)
   ([(control c) (j)] . counsel-git-grep)
   ([(control c) (l)] . counsel-locate)
   ([(control d)] . swiper-ag)
   ([(control f)] . swiper)
   ([(control h) (control f)] . counsel-describe-function)
   ([(control h) (control v)] . counsel-describe-variable)
   ([(control o)] . counsel-find-file)
   ([(control shift o)] . counsel-recentf)
   ([f9] . counsel-rhythmbox)
   :map ivy-minibuffer-map
   ([(control f)] . ivy-yank-word)
   :map read-expression-map
   ([(control r)] . counsel-expression-history))
  :config
  (progn
    (define-key
      ivy-switch-buffer-map
      [(control w)]
      (lambda ()
        (interactive)
        (ivy-set-action 'kill-buffer)
        (ivy-done))))
  :init
  (setq  ivy-use-virtual-buffers t
         ivy-ignore-buffers '("\\` " "\\`\\*")
         ivy-display-style 'fancy
         ivy-count-format "(%d/%d) "
         ivy-initial-inputs-alist nil
         ivy-extra-directories nil))

(use-package try
  :ensure t)

(use-package aggressive-indent
  :ensure t
  :diminish "grr"
  :init
  (add-hook 'web-mode-hook #'aggressive-indent-mode)
  (add-hook 'js-mode-hook #'aggressive-indent-mode)
  (add-hook 'css-mode-hook #'aggressive-indent-mode)
  (add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode))

(use-package which-key
        :ensure t
        :config
        (which-key-mode))

(use-package avy
  :ensure t
  :bind
  ("C-S-j" . avy-goto-word-1))

(use-package package-build
  :ensure t)

(use-package flycheck-package
  :ensure t
  :config
  (add-hook 'js-mode-hook 'flycheck-mode)
  (setq flycheck-jshintrc "~/.jshintrc.json"
        flycheck-javascript-jshint-executable "~/.nvm/versions/node/v8.5.0/bin/jshint"))

(use-package undo-tree
  :bind
  ("C-z" . undo-tree-undo)
  ("C-S-z" . undo-tree-redo)

  :diminish ut
  :init
  (global-undo-tree-mode)
  :config
  (setq
   undo-tree-history-directory-alist (quote (("." . "~/.emacs.d/backup/")))
   undo-tree-visualizer-timestamps t
   undo-tree-visualizer-diff t
   undo-tree-auto-save-history t))

(use-package yasnippet
  :ensure t
  :bind
  ("TAB" . indent-for-tab-command)
  ("<M-tab>" . yas-expand)
  :config
  (yas-global-mode t)
  (add-hook 'minor-mode-hook
            '(lambda ()
               (add-to-list 'yas-extra-modes 'minor-mode)
               (yas-minor-mode-on)))
  (make-variable-buffer-local 'yas-extra-modes))

(use-package paredit
  :ensure t
  :bind
  ([(meta shift left)] . paredit-forward-slurp-sexp)
  ([(meta shift right)] . paredit-forward-barf-sexp)
  ([(meta s)] . save-buffer)
  ([(deletechar)] . delete-forward-char)
  :config
  (define-key emacs-lisp-mode-map (kbd "(") 'paredit-open-round)
  (define-key emacs-lisp-mode-map (kbd "\"") 'paredit-doublequote)
  ;; :init
  ;; (add-hook 'emacs-lisp-mode-hook #'paredit-mode)
  ;; ;; enable in the *scratch* buffer
  ;; (add-hook 'lisp-interaction-mode-hook #'paredit-mode)
  )

(use-package redshank
  :commands redshank-mode
  :diminish redshank-mode
  :config
  (use-package paredit))

(use-package tabbar
  ;; :disabled t
  :ensure t
  :bind
  ("<C-S-iso-lefttab>" . tabbar-backward)
  ("<C-tab>" . tabbar-forward)

  :config
  (set-face-attribute
   'tabbar-button nil
   :box '(:line-width 1 :color "gray19"))

  (set-face-attribute
   'tabbar-selected nil
   :foreground "orange"
   :background "gray19"
   :box '(:line-width 1 :color "gray19"))

  (set-face-attribute
   'tabbar-unselected nil
   :foreground "gray75"
   :background "gray25"
   :box '(:line-width 1 :color "gray19"))

  (set-face-attribute
   'tabbar-highlight nil
   :foreground "black"
   :background "orange"
   :underline nil
   :box '(:line-width 1 :color "gray19" :style nil))

  (set-face-attribute
   'tabbar-modified nil
   :foreground "orange red"
   :background "gray25"
   :box '(:line-width 1 :color "gray19"))

  (set-face-attribute
   'tabbar-selected-modified nil
   :foreground "orange red"
   :background "gray19"
   :box '(:line-width 1 :color "gray19"))

  ;;   (set-face-attribute
  ;;    'tabbar-button nil
  ;;    :inherit 'tabbar-default)

  (custom-set-variables
   '(tabbar-separator (quote (0.2))))

  ;; Change padding of the tabs
  ;; we also need to set separator to avoid overlapping tabs by highlighted tabs
  ;; (custom-set-variables
  ;;  '(tabbar-separator (quote (1.0))))
  (defun tabbar-buffer-tab-label (tab)
    "Return a label for TAB.
  That is, a string used to represent it on the tab bar."
    (let ((label  (if tabbar--buffer-show-groups
                      (format " [%s] " (tabbar-tab-tabset tab))
                    (format " %s " (tabbar-tab-value tab)))))
      ;; Unless the tab bar auto scrolls to keep the selected tab
      ;; visible, shorten the tab label to keep as many tabs as possible
      ;; in the visible area of the tab bar.
      (if tabbar-auto-scroll-flag
          label
        (tabbar-shorten
         label (max 1 (/ (window-width)
                         (length (tabbar-view
                                  (tabbar-current-tabset)))))))))

  (defun px-tabbar-buffer-select-tab (event tab)
    "On mouse EVENT, select TAB."
    (let ((mouse-button (event-basic-type event))
          (buffer (tabbar-tab-value tab)))
      (cond
       ((eq mouse-button 'mouse-2) (with-current-buffer buffer (kill-buffer)))
       ((eq mouse-button 'mouse-3) (pop-to-buffer buffer t))
       (t (switch-to-buffer buffer)))
      (tabbar-buffer-show-groups nil)))

  (defun px-tabbar-buffer-help-on-tab (tab)
    "Return the help string shown when mouse is onto TAB."
    (if tabbar--buffer-show-groups
        (let* ((tabset (tabbar-tab-tabset tab))
               (tab (tabbar-selected-tab tabset)))
          (format "mouse-1: switch to buffer %S in group [%s]"
                  (buffer-name (tabbar-tab-value tab)) tabset))
      (format "\
mouse-1: switch to %S\n\
mouse-2: kill %S\n\
mouse-3: Open %S in another window"
              (buffer-name (tabbar-tab-value tab))
              (buffer-name (tabbar-tab-value tab))
              (buffer-name (tabbar-tab-value tab)))))

  (defun px-tabbar-buffer-groups ()
    "Sort tab groups."
    (list (cond ((or
                  (eq major-mode 'dired-mode)
                  (string-equal "*" (substring (buffer-name) 0 1))) "emacs")
                (t "user"))))
  (setq tabbar-help-on-tab-function 'px-tabbar-buffer-help-on-tab
        tabbar-select-tab-function 'px-tabbar-buffer-select-tab
        tabbar-buffer-groups-function 'px-tabbar-buffer-groups)

  ;; (add-hook 'after-change-major-mode-hook
  ;;           '(lambda ()
  ;;              (tabbar-mode (if (or
  ;;                                (equal major-mode 'help-mode)
  ;;                                (equal major-mode 'recentf-dialog-mode))
  ;;                               -1
  ;;                             1))))

  :init
  (tabbar-mode 1)
  )

(use-package moe-theme
  :ensure t
  :config
  (progn
    (load-theme 'moe-dark :no-confirm)
    ;; (set-face-attribute 'linum nil :background "gray19")
    (set-face-attribute 'fringe nil :background "gray19")))

;; (use-package org-bullets
;;   :ensure t
;;   :config
;;   (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

;; Funcs

(defun px-insert-or-enclose-with-signs (leftSign rightSign)
  "Insert a matching bracket and place the cursor between them."
  (interactive)
  (if mark-active
      (let ((st (point))
            (ed (mark)))
        (goto-char ed)
        (save-excursion
          (if (> st ed)
              (progn
                (insert leftSign)
                (goto-char st)
                (forward-char 1)
                (insert rightSign))
            (progn
              (insert rightSign)
              (goto-char st)
              (insert leftSign)
              (goto-char (+ 1 ed)))))
        (if (> st ed)
            (goto-char (+ 2 st))
          (goto-char (+ 2 ed))))
    (progn
      (insert leftSign rightSign)
      (backward-char 1))))

(defun insert-pair-paren () (interactive) (px-insert-or-enclose-with-signs "(" ")"))
(defun insert-pair-jquery () (interactive) (px-insert-or-enclose-with-signs "$(" ")"))
(defun insert-pair-brace () (interactive) (px-insert-or-enclose-with-signs "{" "}"))
(defun insert-pair-brace-and-return () (interactive) (px-insert-or-enclose-with-signs "{" "}")
       (newline 2)
       ;; (newline-and-indent)
       (indent-for-tab-command)
       (forward-line -1)
       )

(defun insert-pair-bracket () (interactive) (px-insert-or-enclose-with-signs "[" "]"))
(defun insert-pair-single-angle () (interactive) (px-insert-or-enclose-with-signs "<" ">"))
(defun insert-pair-squote () (interactive) (px-insert-or-enclose-with-signs "'" "'"))
(defun insert-pair-dbquote () (interactive) (px-insert-or-enclose-with-signs "\"" "\""))

(defun px-unfill-paragraph (&optional region)
  "Takes a multi-line paragraph and makes it into a single line of text."
  (interactive (progn
                 (barf-if-buffer-read-only)
                 (list t)))
  (let ((fill-column (point-max)))
    (fill-paragraph nil region)))

(defun px-flash-region (start end &optional timeout)
  "Temporarily highlight region from START to END."
  (let ((overlay (make-overlay start end)))
    (overlay-put overlay 'face 'secondary-selection)
    (run-with-timer (or timeout 0.2) nil 'delete-overlay overlay)))

(defun delete-grep-header (orig-fun &rest args)
  (message "fn called with args %S" args)
  (let ((res (apply orig-fun args)))
    (save-excursion
      (with-current-buffer "*ag search*"
        (goto-line 5)
        (narrow-to-region (point) (point-max))))
    res))

;; (advice-add 'projectile-ag :after #'delete-grep-header)

;; (defadvice grep (after projectile-ag activate) (delete-grep-header))
;; (defadvice rgrep (after delete-grep-header activate) (delete-grep-header))

(defadvice kill-ring-save (before slick-copy activate compile)
  "When called interactively with no active region, COPY a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (px-flash-region (line-beginning-position) (line-beginning-position 2))
     (message "Copied line")
     (list (line-beginning-position)
           (line-beginning-position 2)))))

(defadvice kill-region (before slick-cut activate compile)
  "When called interactively with no active region, KILL a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (message "Killed line")
     (list (line-beginning-position)
           (line-beginning-position 2)))))

(defun px-toggle-comments ()
  "If region is set, [un]comments it. Otherwise [un]comments current line."
  (interactive)
  (if (eq mark-active nil)
      (progn
        (beginning-of-line 1)
        (set-mark (point))
        (forward-line)
        (comment-dwim nil))
    (comment-dwim nil))
  (deactivate-mark))

(defun px-kill-other-buffers ()
  "Kill all other buffers."
  (interactive)
  (mapc 'kill-buffer (delq (current-buffer) (buffer-list))))

(defun px-scratch ()
  "Switch to scratch buffer"
  (interactive)
  (switch-to-buffer "*scratch*"))

(defun move-line-up ()
  "Move up the current line."
  (interactive)
  (transpose-lines 1)
  (forward-line -2)
  (indent-according-to-mode))

(defun move-line-down ()
  "Move down the current line."
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1)
  (indent-according-to-mode))

(defun px-websearch (start end)
  "Websearch selected string, or word under point."
  (interactive (if (use-region-p)
                   (list (region-beginning) (region-end))
                 (remove nil
                         (save-excursion
                           (list
                            (backward-sexp) (point)
                            (forward-sexp) (point))))))

  (let ((selection (buffer-substring-no-properties start end)))
    (px-flash-region start end)
    (browse-url (concat "https://google.com/?q="
                        (url-hexify-string selection) "!g"))))

(defun px-select-text-in-quotes ()
  "Select text between the nearest left and right delimiters."
  (interactive)
  (let (start end)
    (skip-chars-backward "^<>([{\"'")
    (setq start (point))
    (skip-chars-forward "^<>)]}\"'")
    (setq end (point))
    (set-mark start)))

(defun px-insert-or-enclose-with-signs (leftSign rightSign)
  "Insert a matching bracket and place the cursor between them."
  (interactive)
  (if mark-active
      (let ((st (point))
            (ed (mark)))
        (goto-char ed)
        (save-excursion
          (if (> st ed)
              (progn
                (insert leftSign)
                (goto-char st)
                (forward-char 1)
                (insert rightSign))
            (progn
              (insert rightSign)
              (goto-char st)
              (insert leftSign)
              (goto-char (+ 1 ed)))))
        (if (> st ed)
            (goto-char (+ 2 st))
          (goto-char (+ 2 ed))))
    (progn
      (insert leftSign rightSign)
      (backward-char 1))))

(defun px-insert-end-of-command-sign ()
  "Insert a ; at the end of the current line."
  (interactive)
  (let ((st (point)))
    (save-excursion
      (move-end-of-line nil)
      (insert ";")
      (goto-char st))))

(defun insert-pair-paren () (interactive) (px-insert-or-enclose-with-signs "(" ")"))
(defun insert-pair-brace () (interactive) (px-insert-or-enclose-with-signs "{" "}"))
(defun insert-pair-brace-and-return () (interactive) (px-insert-or-enclose-with-signs "{" "}")
       (newline-and-indent)
       (newline-and-indent)
       (forward-line -1))

(defun last-edit ()
  "Go back to last add/delete edit"
  (interactive)
  (let* ((ubuf (cadr buffer-undo-list))
         (beg (car ubuf))
         (end (cdr ubuf)))
    (cond
     ((integerp beg) (goto-char beg))
     ((stringp beg) (goto-char (abs end))
      (message "DEL-> %s" (substring-no-properties beg)))
     (t (message "No add/delete edit occurred")))))

(defun my-tidy ()
  "Automatically re-indents code"
  (interactive)
  (execute-kbd-macro
   (read-kbd-macro "C-u -999 M-x indent-region RET C-x C-x M-x indent-region")))

(global-set-key (kbd "C-M-*") 'my-tidy)


;; Notes
;; C-x = gives the current point value
;; C-M-x before a sexp evals it, but
;; C-u C-M-x wraps edebug around it :
;; Space to advance, C to continue, and I? RTFM
;; elint-initialize, then elint-current-buffer to go further than byte-compile
;; C-x v = diffs the buffer against the last commit
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-image-file-mode nil)
 '(package-selected-packages
   (quote
    (ag ensime scala-mode flymake-jslint flycheck-jslint mustache-mode flymake-json projectile handlebars-mode json-mode yasnippet which-key web-mode use-package undo-tree try tabbar redshank rainbow-mode php-mode paredit package-build moe-theme markdown-mode magit js2-mode ivy-hydra iedit flycheck-package flycheck-cask emmet-mode elisp-slime-nav counsel beacon avy aggressive-indent)))
 '(tabbar-separator (quote (0.2))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(linum ((t (:background "gray19" :foreground "#b2b2b2")))))
