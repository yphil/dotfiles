#!/bin/bash

# zenity --entry --entry-text="$NAUTILUS_SCRIPT_SELECTED_FILE_PATHS"

# smplayer -add-to-playlist $NAUTILUS_SCRIPT_SELECTED_FILE_PATHS

# for file in $NAUTILUS_SCRIPT_SELECTED_FILE_PATHS
#    do
#      smplayer -add-to-playlist "$file"
# done

echo "$NAUTILUS_SCRIPT_SELECTED_FILE_PATHS" | while read filename; do   

    if [ ${filename:-null} = null ]; then
        echo "line is empty"
    else
        smplayer -add-to-playlist "$filename"
    fi
done
