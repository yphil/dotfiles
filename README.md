# Dotfiles

Environment (dotfiles, aliases and commands, scripts, Debian packages, Mozilla extensions, etc.) installation & update sh (tested w/ BASH and ZSH) script

## Lazy me

What do one need on a given computer ? Preferences.

- Dotfiles ("hidden" configuration files in your `$HOME` or `$HOME/.config/<program name>/config_file` dir. It's a **big deal** ; GNU/Linux programs [work like this](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html)) ;
- a selection of packages depending on what I intend to do with that rig. And emacs :)

AFAIC, that's it. And there's only so many times a guy can type "install this", "install that", `mkdir ~/tmp`, and many bits that get painful to do and re-do over time, before doing something about it.

So on **all** my machines, I run this script (heck, even my phone) basically at boot time.

**NOTA BENE** You can "preview" the script by answering "no" to every question, and then get a taste of what would be going on without touching anything. The idea here is to answer "Yes" (ENTER) and that everything goes smoothly.

## Features

- Install and maintain the things I (and you too I guess) need on every machine I install :
    - Shell (I use ZSH, but I keep all my `.bash*` files compatible : I source the same functions, env vars and aliases) files ;
    - Binary packages (debian - & friends - only, sorry. I mean of course I can detect your distro - in the dark - but every single one has its own package naming scheme, what are you gonna do?)
    - Various scripts from Version Controlled repositories (currently none, since that Emacs now handles its own packages, installed at startup) ;
    - Mozilla (Firefox, Thunderbird, ABrowser, whatever) add-ons ;
- Manage multiple VC systems
    - GIT ;
    - SVN ;
    - BZR ;
    - CVS ;
    - Anything that's three-letter named :)
- Allow editing of the files as well as **deployment/syncing on every other machine** ;
- Setup a home dir (`~/tmp`, `~/.bin`, that sort of stuff) the way I'm used to.

## Installation

```sh
cd
git clone https://framagit.org/yphil/dotfiles.git .dotfiles && .dotfiles/.scripts/dotfiles-setup.sh
```

Or, to install it and be able to modify it and push your changes (if you're me, for instance)

```sh
git clone git@framagit.org:yphil/dotfiles.git .dotfiles && .dotfiles/.scripts/dotfiles-setup.sh -rw
```

- Then run `dotfiles-setup.sh -rw` afterwards to install/clone your own VC repositories in read-write mode (`dotfiles-setup.sh` to just update)

This will :

- Clone a repository at `~/.dotfiles/`
- Backup all existing `~/.dotfile` as `~/.dotfile.orig` (well, not *all*, only those that are actually versioned in this repo) as well as `~/.config/*/*` config files, to ensure a smooth transition towards a clean homedir ;
- Symlink `~/.dotfiles/*` in `~/*`
  - (Now you have a new dir, `~/.scripts/` and it's in your `$PATH`)
- Install a selection of packages, sorted by groups: Multimedia, Dev, Music prod, etc. You're asked for each group if you want to install.
- Generate a local temp web page on the fly, allowing quick install of - the latest version of - my/your selection of Firefox extensions
- Update everything to the latest version at each subsequent run

## LICENSE

This program is free software: you can redistribute it and/or modify it under the terms of [the GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see http://www.gnu.org/licenses.
