# New machine? Remember to

## Set crontabs
`(crontab -l 2>/dev/null; echo "0 * * * * ${MYSCRIPTDIR}/px-i3-next-tide-time.sh") | crontab -`

## Set SMplayer prefs
Volume keys
Arrow keys

## Set keyboard modifiers
Open IBus and REMOVE the Ctrl modifier

## Fstab
LABEL=label     /mnt/label   auto nosuid,noatime,nodev,nofail      0 2

## Apt
sudo apt install --only-upgrade <packages that have been kept back>

## Beach
put this:

`plage="https://www.cabaigne.net/<location>"`

in a file named `~/.local_vars.txt`
