# SuberCool

![subercool.png](./images/subercool.png)

This is the documentation for [SuberCool](https://framagit.org/yphil/dotfiles/-/blob/master/.scripts/subercool.py).



## Installation
```shell
sudo apt install python3 exiftool
pip install zenipy 
wget https://framagit.org/yphil/dotfiles/-/raw/master/.scripts/subercool.py -O ~/.local/bin/subercool.py
```

### Nautilus plugin
Then to install it as a [Nautilus](https://apps.gnome.org/app/org.gnome.Nautilus/) plugin:
```shell
mkdir -p ~/.local/share/nautilus/scripts
ln -s ~/.local/bin/subercool.py ~/.local/share/nautilus/scripts/Download__Subtitles
mkdir -p ~/.local/share/nautilus/scripts
echo "<Control><Shift>s Download__Subtitles" >> ~/.local/share/nautilus/scripts
```

## Configuration
Follow [the instructions](https://opensubtitles.stoplight.io/docs/opensubtitles-api/) to get an api key (it's quite simple really) which is a simple string of 32 characters.
Then, either pass this string as an argument like this:

```shell
subercool.py -k API_KEY ~/Videos/MyMovie.mp4
```

Or (simpler, and mandatory for the Nautilus plugin mode) put in in a file named `~/.local_variables.json` like this:

```json ~/.local_variables.json
{
    "opensubtitles_api_key": "API_KEY"
}

```

## Usage
```shell subercool.py -h
usage: subercool.py [-h] [-k [API_KEY]] file

Download video subtitle(s)

positional arguments:
  file                  File to search in the opensubtitles database

options:
  -h, --help            show this help message and exit
  -k [API_KEY], --api_key [API_KEY]
                        API key to the opensubtitles database

See full documentation here: https://framagit.org/yphil/dotfiles/-/blob/master/docs/SuberCool.md
```

Brought to you by yPhil ; Please (please?) consider [helping](https://liberapay.com/yPhil/donate) ❤️
