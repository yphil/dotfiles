#!/usr/bin/env bash

# This is way south of perfection

DEVICE="/dev/input/event22" # Everything-is-a-file :)

monitor_events() {
    echo "Listening for events on $DEVICE..."
    evtest "$DEVICE" | while read line; do
        if [[ "$line" == *"code 314 (BTN_SELECT), value 0"* ]]; then
            echo "BTN_SELECT button released"
        fi

        if [[ "$line" == *"code 308 (BTN_WEST), value 0"* ]]; then
            echo "BTN_WEST (square) button released"

            if pgrep -f wiicube > /dev/null; then
                echo "wiicube is already running. Exiting..."
                exit 1
            else
                wiicube --no-sandbox
            fi

        fi

        if [[ "$line" == *"code 304 (BTN_SOUTH), value 0"* ]]; then
            echo "BTN_SOUTH (circle) button released"
        fi
    done
}

# Main loop
while true; do
    # Check if wiicube is running; skip monitoring if it is
    if pgrep -f wiicube > /dev/null; then
        echo "WiiCube is running. Not monitoring events."
        sleep 5  # Wait before checking again
        continue
    fi

    # Check if the device exists
    if [[ -e $DEVICE ]]; then
        monitor_events
    else
        echo "Controller not connected. Waiting..."
        # Poll for device reconnect (adjust sleep if needed)
        while [[ ! -e $DEVICE ]]; do
            sleep 1
        done
        echo "Controller reconnected!"
    fi
done
