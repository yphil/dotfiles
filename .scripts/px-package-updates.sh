#! /usr/bin/env bash

# Copyright (C) 2020 yphil <xaccrocheur@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#------------------------------------------------------------------------

fullList=$(apt list --upgradable 2> /dev/null)
shortList=$(echo "$fullList" | cut -f1 -d"/" | sed s/Listing...//)
tmpDir=~/tmp/changelogs/
changelogs=""
PAGE=${tmpDir}changelogs.html

mkdir -p ${tmpDir}
rm -f ${tmpDir}/*

u="$(LC_ALL=fr_FR.utf8 date -u +"%c,%H:%M,%d/%m/%Y,%Z (%:z)")"
IFS=','
set -- $u
# echo "${1}"

for pkg in $shortList ; do
    # echo $pkg
    apt-get changelog ${pkg} > "${tmpDir}${pkg}.changelog"
        apt-get changelog ${pkg} | head -15
    # changelogs=$changelogs"<li><a href='"${tmpDir}${pkg}".changelog'>$pkg</a> (<a href='https://duckduckgo.com/?q="${pkg}"'>info</a>)</li>\n"
done

#     echo -e "
# <html>
# <head>
#   <title>Package updates - $(hostname)</title>
# </head>
#   <body style='font-family: sans-serif'>
#   <h1>Package updates</h1>
#   <h2>$(date +'%T - %d/%m/%Y') - $(hostname)</h2>
#    <ul>"
#     $changelogs
#    "</ul>
#   <hr />
# </body>
# </html>" >> $PAGE

# echo $changelogs

echo "## Done generating ${PAGE}"
