#!/bin/env bash

fg1="#FFFFFF" # White
fg2="#5F8787" # PaleTurquoise4
fg3="#90A959" # Aquamarine3
fg4="#EEEEEE" # Grey93
bg1="#808080" # Grey50
bg2="#005f87" # DeepSkyBlue4
bg3="#5f5f87" # MediumPurple4
bg4="#AFAF87" # NavajoWhite
hi1="#FF8700" # DarkOrange
hi2="#FF5F00" # OrangeRed1
hi3="#AFFFD7" # DarkSeaGreen1
hi4="#D70000" # Red3
lo1="#8787AF" # LightSlateGrey
lo2="#303030" # Grey19
lo3="#262626" # Grey15
lo4="#000000" # Black

case "$1" in
    fg1) echo $fg1 ;;
    fg2) echo $fg2 ;;
    fg3) echo $fg3 ;;
    fg4) echo $fg4 ;;
    bg1) echo $bg1 ;;
    bg2) echo $bg2 ;;
    bg3) echo $bg3 ;;
    bg4) echo $bg4 ;;
    hi1) echo $hi1 ;;
    hi2) echo $hi2 ;;
    hi3) echo $hi3 ;;
    hi4) echo $hi4 ;;
    lo1) echo $lo1 ;;
    lo2) echo $lo2 ;;
    lo3) echo $lo3 ;;
    lo4) echo $lo4 ;;
    *) echo "Usage: $0 {color_code}"
esac

awk 'BEGIN{
    s="/\\/\\/\\/\\/\\"; s=s s s s s s s s;
    for (colnum = 0; colnum<77; colnum++) {
        r = 255-(colnum*255/76);
        g = (colnum*510/76);
        b = (colnum*255/76);
        if (g>255) g = 510-g;
        printf "\033[48;2;%d;%d;%dm", r,g,b;
        printf "\033[38;2;%d;%d;%dm", 255-r,255-g,255-b;
        printf "%s\033[0m", substr(s,colnum+1,1);
    }
    printf "\n";
}'
