#!/bin/env python3

import re
import os
import argparse
import html

parser = argparse.ArgumentParser()

parser.add_argument("-t", "--formatting", help = "Format of the output: html (default) / text")
parser.add_argument("-o", "--outtype", help = "Output type: stdout (default) / file / full (both)")
parser.add_argument("-f", "--outfile", help = "Temp. out file PATH (defaults to /tmp/.i3-kb-bindings.html)")

args = parser.parse_args()

if args.outtype:
    outtype = args.outtype
else:
    outtype = 'stdout'

if args.outfile:
    outFile = args.outfile
else:
    outFile = os.path.expanduser('/tmp/.i3-kb-bindings.html')
    
if args.formatting:
    formatting = args.formatting
else:
    formatting = 'text'

f = open('/home/px/.dotfiles/config/i3/config','r')

TMPFILE = "/tmp/.i3-kb-bindings.html"

pat = '# MODE:\n(.*?)\}'
pot = 'bindsym(.*?)mode \"\$'
pit = '{|bindsym|exec |--no-startup-id |\, mode \"default\"'

content = f.read()

match = re.findall(pat,content,re.MULTILINE|re.DOTALL)

nbOfModes = 0
nbOfModeKeys = 0
modes = []
simpleKeys = []

def formatKey(keyString, formatting):
    thisModeKey = keyString.split(' ')[0]
    thisCommand = keyString.split(' ', 1)[1]
    if formatting == 'html':
        return '<tr><td class="key"><span class="key">' + thisModeKey + '</span></td> <td><span class="cmd">' + html.escape(thisCommand) + '</span></td></tr>\n'
    else:
        return thisModeKey + ' : ' + thisCommand

for i in match:

    result = re.split(pot, i)
    nbOfModes = len(result) + 1
    result.pop(0)

    result[0] = result[0].strip()
    result[1] = result[1].replace('\n( +?)\n', '\n')
    result[1] = result[1][result[1].find('{'):]
    result[1] = re.sub(pit, '', result[1])
    
    lst = result[1].split('\n')
    lst[0] = result[0]

    lst = [i.strip(' ') for i in lst]
    length = len(lst) 
    i = 0

    mode = []
    
    nbOfModeKeysInThisMode = 0
    while i < length:
        if lst[i].startswith('$'):
            mode.append(lst[i])
            nbOfModes += 1
        if lst[i]:
            if not lst[i].startswith('#') and not lst[i].startswith('$'):
                mode.append(lst[i])
        i += 1
    modes.append(mode)
    
simpleKeysRepat = '^bindsym.*'

simpleKeysRe = re.findall(simpleKeysRepat, content, flags=re.MULTILINE)    

for simpleKey in simpleKeysRe:
    simpleKey = re.sub(pit, '', simpleKey).strip()
    x = re.search("button", simpleKey)
    if x:
        pass
    else:
        simpleKeys.append(simpleKey)
        
style = '<style> body {background-color: #202020;} td.key {width: 100px} h2, h3 {color: white;} h3 {margin:0 10px -10px 0; text-align: right} th {color: white} tt, .key {color: Chartreuse} th {text-align: left} .cmd {color: DeepSkyBlue} i {color: red;} </style>'

table = '<table>'

with open(TMPFILE, 'w') as the_file:
    the_file.write(style + table)

for simpleKey in simpleKeys:
    if args.formatting == 'html':
        if args.outtype == 'file':
            with open(TMPFILE, 'a') as the_file:
                the_file.write(formatKey(simpleKey, 'html'))
        else:
            print(formatKey(simpleKey, 'html'))
    else:
        if args.outtype == 'stdout':
            print(formatKey(simpleKey, 'text'))
     
with open(TMPFILE, 'a') as the_file:
    the_file.write(' </table>' + '<table>')

for mode in modes:
    CAR = mode[0].split()[0].strip()
    if args.outtype == 'stdout':
        print('\nMODE:' + CAR)
    else:
        with open(TMPFILE, 'a') as the_file:
            the_file.write('<h3 class="key">' + CAR + '</h3>' + table)
    mode.pop(0)
    for key in mode:
        if args.formatting == 'html':
            with open(TMPFILE, 'a') as the_file:
                the_file.write(formatKey(key, 'html'))
        else:
            if args.outtype == 'stdout':
                print(formatKey(key, 'text'))
    with open(TMPFILE, 'a') as the_file:
        the_file.write('</table>')
