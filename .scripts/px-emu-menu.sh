#!/usr/bin/env bash

APPDIR="$HOME/bin/EMU"

ICONDIR="$APPDIR/.icons"

MENU_ITEMS=""
for app in "$APPDIR"/*.AppImage; do
    appname=$(basename "$app" .AppImage)
    iconName=$(echo "$appname" | awk -F'-' '{print $1}')
    # icon="$ICONDIR/$iconName.png"
    # icon="$ICONDIR/$iconName.${iconName:+(png|svg)}"
    icon="$ICONDIR/$iconName.png"
    [ ! -f "$icon" ] && icon="$ICONDIR/$iconName.svg"

    if [ -f "$icon" ]; then
        MENU_ITEMS+="${iconName}\0icon\x1f${icon}\n"  # icon and label
    else
        MENU_ITEMS+="${iconName}\n"  # label without icon
    fi
done

SELECTED=$(echo -en "$MENU_ITEMS" | rofi -dmenu -show-icons)

if [ -n "$SELECTED" ]; then
    SELECTED_APP="$APPDIR/$SELECTED.AppImage"

    if [ -f "$SELECTED_APP" ]; then
        "$SELECTED_APP" &
    else
        notify-send "AppImage Launcher" "Error: AppImage not found!"
    fi
fi
