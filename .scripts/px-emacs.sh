# -*-Shell-script-*-

#! /usr/bin/env bash

# Copyright (C) 2020 yphil <xaccrocheur@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#------------------------------------------------------------------------
# [[ $(pidof "emacs") ]] && emacsclient ${1} > /dev/null || emacs ${1}

if [ "$(pidof emacs)" ] ; then
    if [ $# -eq 0 ] ; then
        echo "wopopop"
        wmctrl -x -a "emacs"
    else
        emacsclient "$@" &
    fi
else
    if [ $# -eq 0 ] ; then
        emacs &
    else
        emacs "$@" &
    fi
fi


# pidof smplayer >/dev/null && echo "Service is running" || echo "Service NOT running"
