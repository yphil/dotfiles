#!/usr/bin/env bash


read -d '' URLS <<"EOF"
https://exode.me/videos/watch/fed67262-6edb-4d1c-833b-daa9085c71d7
https://exode.me/videos/watch/271cd39f-e641-40b9-9fc9-8335ac19cbcb
https://exode.me/videos/watch/46ace0b0-c1a4-4aa9-8377-2bc91641fdd8
https://exode.me/videos/watch/eba58662-ea38-497c-8f5c-7be4a5df17c9
https://exode.me/videos/watch/3bb3bc88-7033-4397-8a77-cec322b62192
https://exode.me/videos/watch/0f93b69d-b571-4b27-b77d-e7f523de5048
https://exode.me/videos/watch/a8d03913-c7b4-4643-bd77-ada68e08e310
https://exode.me/videos/watch/36348b8e-f612-4825-ace2-fe3eec28fdd6
https://exode.me/videos/watch/33a0f406-f1f9-4fc6-948c-fa2019141661
https://exode.me/videos/watch/620cf53c-779b-4e24-bb03-9737b54e08f2
https://exode.me/videos/watch/8c4cc47d-1fb4-4b2f-bf9e-1d273f65e326
https://exode.me/videos/watch/52d4e2ff-32a5-4f62-b2e4-3ff6a94f0752
https://exode.me/videos/watch/2795111d-0cba-4c0d-b3de-b1bebf8d3d1e
https://exode.me/videos/watch/e0445ef1-dd26-4c00-8a51-d8197f4faa1c
https://exode.me/videos/watch/c22a1983-a3b3-4c61-ad41-61d9d224e611
https://exode.me/videos/watch/23cefafa-249e-455f-b134-c3459ed2890f
https://exode.me/videos/watch/4117b34d-bfdf-439e-84a1-b1e7ee59be9f
https://exode.me/videos/watch/e78a01ed-bafa-4feb-91c5-59d779e199f3
https://exode.me/videos/watch/fed67262-6edb-4d1c-833b-daa9085c71d7
https://exode.me/videos/watch/6e77fd22-b4f1-452f-82e6-cd7e1bf2bd7b
https://exode.me/videos/watch/d6366183-d98c-49c4-8767-299407e1c915
https://exode.me/videos/watch/41b781be-1167-4d41-8f3e-bcbc0bec1149
EOF

for URL in $URLS ; do
    RDM=$((120 + RANDOM % 240))
    echo "Getting ${URL} ($(date +"%T"), sleeping for ${RDM} secs)"
    curl --output /dev/null --silent -H "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0" ${URL}
    sleep $RDM
done
