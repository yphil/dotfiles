#!/usr/bin/env bash

FILE="${1:-}"

if [[ -e "$FILE.mkv" ]] ; then
    read -e -p "## File ($FILE) exists, delete? [y/N] " YN
    if [[ $YN == "n" || $YN == "N" || $YN == "" ]] ; then
        exit
    else
        rm -rf "$FILE"*
    fi
fi

# key-mon --visible_click -l --backgroundless -t modern &
# ffmpeg -f x11grab -r 25 -s 1855x1200 -i :0.0+65,0 "$FILE.mp4" &
# ffmpeg -f x11grab -r 30 -s 1920x1200 -i :0.0 -vcodec libx264 -preset ultrafast -crf 0 "$FILE.mkv"

# ffmpeg -f x11grab -r 30 -i :0.0 -vcodec libx264 -preset ultrafast -crf 0 "$FILE.mkv"

ffmpeg -video_size 1920x1200 -framerate 30 -f x11grab -i :0.0 "$FILE.mkv"

# ffmpeg -f x11grab -r 30 "$FILE.mkv"

# ffmpeg -f x11grab -r 30 "$FILE.mp4" &
# jack_capture "$FILE.wav"
# jack_capture -c 2 -p system:capture_1 -p Qtractor:Master/out_1 -p Qtractor:Master/out_2 "$FILE.wav"
# wait
