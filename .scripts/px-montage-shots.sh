#!/bin/bash

if [ $# -eq 0 ]; then
  echo "Usage: $0 <name>"
  exit 1
fi

name="$1"
output="montage-$name.png"
margin=12

# Function to create montage
function create_montage() {
  local input_images=("$@")
  montage_cmd="montage"
  for image in "${input_images[@]}"; do
    montage_cmd+=" $image -geometry +${margin}+${margin}"
  done
  montage_cmd+=" -background transparent -tile x1 -geometry +${margin}+${margin} $output"
  eval "$montage_cmd"
}

# Find all files matching the pattern
matching_files=($(find . -maxdepth 1 -type f -name "$name-[0-9]*.png" | sort -V))

# Check if any files were found
if [ ${#matching_files[@]} -eq 0 ]; then
  echo "No matching files found for '$name'"
  exit 1
fi

# Create the montage
create_montage "${matching_files[@]}"

echo "Montage created with a ${margin}px transparent margin."
