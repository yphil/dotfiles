#!/bin/bash

# Default packages are for the configuration and corresponding .config folders
# Install packages after installing base Debian with no GUI

# xorg display server installation
sudo apt install -y xorg 

# Microcode for Intel/AMD 
# sudo apt install -y amd-microcode
sudo apt install -y intel-microcode 

# Network Manager
sudo apt install -y network-manager-gnome

# Installation for Appearance management
sudo apt install -y lxappearance 

# Network File Tools/System Events
sudo apt install -y avahi-daemon acpi acpid gvfs-backends

sudo systemctl enable avahi-daemon
sudo systemctl enable acpid

# Sound packages
sudo apt install -y pulseaudio alsa-utils pavucontrol

# Printing and bluetooth (if needed)
# sudo apt install -y cups
# sudo apt install -y bluez blueman

# sudo systemctl enable bluetooth
# sudo systemctl enable cups

# Packages needed i3-gaps after installation
sudo apt install -y i3 dmenu i3status i3lock rofi dunst

# Install fonts
sudo apt install fonts-font-awesome fonts-powerline fonts-ubuntu fonts-liberation2 fonts-liberation fonts-terminus fonts-cascadia-code

# Create folders in user directory (eg. Documents,Downloads,etc.)
xdg-user-dirs-update

# Dependencies for Ly Console Manager
# sudo apt install -y libpam0g-dev libxcb-xkb-dev

# # Install Ly Console Display Manager
# cd 
# cd Downloads
# git clone --recurse-submodules https://github.com/nullgemm/ly.git
# cd ly/
# make
# sudo make install
# sudo systemctl enable ly

# Lightdm can be used instead of Ly (more common)
# comment out all ly console display if choosing lightdm
sudo apt install -y lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings
sudo systemctl enable lightdm

# XSessions and i3.desktop
if [[ ! -d /usr/share/xsessions ]]; then
    sudo mkdir /usr/share/xsessions
fi

cat > ./temp << "EOF"
[Desktop Entry]
Encoding=UTF-8
Name=i3
Comment=Dynamic window manager
Exec=i3
Icon=i3
Type=XSession
EOF
sudo cp ./temp /usr/share/xsessions/i3.desktop;rm ./temp

sudo apt autoremove

printf "\e[1;32mYou can now reboot! Thanks you.\e[0m\n"
