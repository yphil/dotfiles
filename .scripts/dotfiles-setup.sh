#!/usr/bin/env bash

DOTDIR=~/.dotfiles
CONFDIR=~/.config
SCRIPTDIR=~/.scripts
REPODIR=~/src
LISPDIR=~/src/lisp

SEP="\n################# "

# Mandatory packages
BASICS="fail2ban goaccess pandoc npm python3-pip zsh vim apt-file curl wget jq htop bc locate git cowsay fortune fortunes-bofh-excuses fortunes-spam p7zip-full links traceroute dnsutils silversearcher-ag dnsutils tmux aspell-fr ssh-askpass lynx pulseaudio-utils"

BASICS_DESKTOP="filezilla yad libxapp-gtk3-module caffeine arandr xdotool input-remapper html-xml-utils tidy alttab universal-ctags mat2 exiftool pavucontrol playerctl sox baobab gparted xclip smplayer xfce4-terminal lxappearance openvpn xarchiver markdown gnome-system-monitor fonts-inconsolata fonts-symbola wmctrl fonts-fork-awesome scrot numlockx rofi zenity redshift-gtk lm-sensors gnome-system-monitor"

# Home dirs
HOME_DIRS="tmp src bin Pictures/CAPS"

# Various repos (cloned & pulled in in $REPODIR)
declare -A REPOS
REPOS[altdesktop/i3ipc-python]="github.com"
REPOS[rupa/z]="github.com"
REPOS[yphil/beatpicker]="bitbucket.org"
REPOS[yphil/fwomaj]="bitbucket.org"
REPOS[zsh-users/zsh-syntax-highlighting]="github.com"
REPOS[raboof/realtimeconfigquickscan]="github.com"

# Various repos (cloned & pulled in in $LISPDIR)
declare -A LISP
LISP[yphil/gpt4emacs]="framagit.org"
LISP[yphil/gpt-intern]="framagit.org"

# Debian packages
declare -A PACKS
PACKS[Dev build tools]="build-essential autoconf devscripts dpkg-dev-el default-jdk"
PACKS[Scala]="scala sbt" # https://www.scala-sbt.org/1.x/docs/Installing-sbt-on-Linux.html
PACKS[Image tools]="gimp gimp-data-extras inkscape"
PACKS[Games]="darkplaces supertux astromenace extremetuxracer supertuxkart chromium-bsu"
PACKS[i3]="i3 dmenu i3status i3lock picom"
PACKS[Music prod]="swami jamin qmidiarp juced-plugins-lv2 teragonaudio-plugins padthv1-lv2 synthv1-lv2 x42-plugins vitalium-lv2 lsp-plugins-lv2 dexed-lv2 qtractor qjackctl kxstudio-recommended-audio-plugins-lv2 qmidinet calf-plugins hexter zam-plugins jalv lilv-utils artyfx fluid-soundfont-gm fluid-soundfont-gs zynaddsubfx helm audacious audacity vmpk cadence lv2-dev pizmidi-plugins oxefmsynth amsynth dpf-plugins qmidiarp rtirq-init vitalium-lv2 distrho-plugin-ports-lv2 swh-lv2 mda-lv2 tal-plugins-lv2 avldrums.lv2 obxd-lv2"

# MOZilla addons
MOZURL="https://addons.mozilla.org/firefox/addon"
declare -A MOZADDONS
MOZADDONS[Scroll Up Folder]="$MOZURL/scroll-up-folder/"
MOZADDONS[uBlock Origin]="$MOZURL/ublock-origin/"
MOZADDONS[Back to Close WE]="$MOZURL/back-to-close-we/"

# Janitor tasks
declare -A TASKS
TASKS[Change timezone]="sudo dpkg-reconfigure tzdata"
TASKS[Set shell to ZSH]="chsh -s /bin/zsh"
TASKS[Add you to the audio group]="sudo adduser $(whoami) audio"
TASKS[Set NPM packages (global) default install dir]="npm config set prefix ~/.local"

# Python packages
PIP="i3ipc ftfy xlib"

# NodeJs (global) packages
NPM="pm2 karma uncss typescript typescript-language-server vscode-json-languageserver"

echo -e $SEP"Dotfiles! #################

Welcome to Dotfiles, $(whoami) ; See README.md for documentation."

echo -e $SEP"Dotfiles and scripts"
hash git 2>/dev/null || { echo >&2 "#### Quickly installing (required) git"; sudo apt install git; }

read -e -p "#### Install / update dotfiles (in $DOTDIR) and scripts (in $SCRIPTDIR)? [Y/n] " YN
if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
    cd $DOTDIR && echo "## $(pwd): " && git pull
    for i in .[^.#]* ; do
         if [[ ! -h ~/${i} && ${i} != ".git" ]] ; then
             [[ -e ~/${i} ]] && mv ~/${i} ~/${i}.orig
             ln -sv ${DOTDIR}/${i} ~/
         fi
     done
 fi

 echo -e $SEP"Server basics"
 read -e -p "#### Install basic packages ($BASICS)? [Y/n] " YN
 [[ $YN == "y" || $YN == "Y" || $YN == "" ]] && sudo apt install ${BASICS}

 echo -e $SEP"Desktop basics"
 read -e -p "#### Install basic packages ($BASICS_DESKTOP)? [Y/n] " YN
 [[ $YN == "y" || $YN == "Y" || $YN == "" ]] && sudo apt install ${BASICS_DESKTOP}

echo -e $SEP"Janitor tasks"
read -e -p "#### Run menial janitor tasks? [Y/n] " YN
if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
     for task in "${!TASKS[@]}" ; do
         read -e -p "
## $task? (${TASKS[$task]})
 [Y/n] " YN
         [[ $YN == "y" || $YN == "Y" || $YN == "" ]] && ${TASKS[$task]}
     done
 fi

read -e -p "
## Create base dirs ($HOME_DIRS)? [Y/n] " YN
if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
     for DIR in $HOME_DIRS ; do
         mkdir -pv ~/$DIR
     done
fi

 read -e -p "
 ## Install Nautilus (Gnome file manager) scripts? [Y/n] " YN
 if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
     mkdir -pv ~/.local/share/nautilus/scripts
     for script in "${DOTDIR}/nautilus_scripts/*" ; do
         script=~/.local/share/nautilus/scripts/$(basename $script)
         [[ -e ${script} ]] && mv ${script} ${script}.orig
         ln -sv ${DOTDIR}/nautilus_scripts/$(basename $script) ~/.local/share/nautilus/scripts/
     done
 fi

echo -e $SEP"Config files"
read -e -p "#### Symlink ($(ls -C $DOTDIR/config)) config files? [Y/n] " YN
if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
    cd "${DOTDIR}/config"
    find . -type f -name '*' | while IFS=$'\n' read -r FILE; do
        # echo $FILE
        THISPATH=$(dirname "${FILE:2}")
        THISBASENAME=$(basename "${FILE:2}")
        [[ ! -d "$CONFDIR/${THISPATH}" ]] && mkdir -vp "$CONFDIR/${THISPATH}"
        [[ ! -h "$CONFDIR/${THISPATH}/${THISBASENAME}" ]] && ln -sfv "$DOTDIR/config/${FILE:2}" "$CONFDIR/${THISPATH}/" || echo -e "## OK (already VC'd) ${FILE:2}"
    done
    cd
fi

echo -e $SEP"VC Repositories"
repos=$(printf "%s, " "${!REPOS[@]}")
read -e -p "#### Install/Update git repos (${repos::-2})? [Y/n] " YN
if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
    for repo in "${!REPOS[@]}" ; do
        echo -e "\n## ${repo##*/}"
        if [[ -d ${REPODIR}/${repo##*/} ]] ; then
            git -C ${REPODIR}/${repo##*/} pull
        else
            if [[ ${repo%%/*} == "yphil" ]] ; then
                git clone git@${REPOS[${repo}]}:${repo}.git ${REPODIR}/${repo##*/}
            else
                git clone https://${REPOS[${repo}]}/${repo}.git ${REPODIR}/${repo##*/}
            fi
        fi
    done
fi

echo -e $SEP"Lisp hacks"
repos=$(printf "%s, " "${!LISP[@]}")
read -e -p "#### Install/Update git repos (${repos::-2})? [Y/n] " YN
if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
    for repo in "${!LISP[@]}" ; do
        echo -e "\n## ${repo##*/}"
        if [[ -d ${LISPDIR}/${repo##*/} ]] ; then
            git -C ${LISPDIR}/${repo##*/} pull origin master
        else
            if [[ ${repo%%/*} == "yphil" ]] ; then
                git clone git@${LISP[${repo}]}:${repo}.git ${LISPDIR}/${repo##*/}
            else
                git clone https://${LISP[${repo}]}/${repo}.git ${LISPDIR}/${repo##*/}
            fi
        fi
    done
fi

echo -e $SEP"NPM/JS stuff"
read -e -p "#### Install NPM/JS packages ($NPM) ? [Y/n] " YN
[[ $YN == "y" || $YN == "Y" || $YN == "" ]] && npm install -g ${NPM}

echo -e $SEP"Python stuff"
read -e -p "#### Install Python packages ($PIP) ? [Y/n] " YN
[[ $YN == "y" || $YN == "Y" || $YN == "" ]] && pip install ${PIP} -break-system-packages

echo -e $SEP"Packages"
read -e -p "#### Install package groups? [Y/n] " YN
if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
    for group in "${!PACKS[@]}" ; do
        read -e -p "
## Install $group? (${PACKS[$group]})
[Y/n] " YN
        [[ $YN == "y" || $YN == "Y" || $YN == "" ]] && sudo apt install ${PACKS[$group]}
    done
fi

echo -e $SEP"Mozilla add-ons"
PAGE=~/tmp/dotfiles-addons.html
ADDONS=""
ADDON_NAMES=""
for ADDON in "${!MOZADDONS[@]}" ; do
    ADDONS=$ADDONS"    <li><a href='"${MOZADDONS[$ADDON]}"'>$ADDON</a></li>\n"
    ADDON_NAMES=$ADDON", "$ADDON_NAMES
done
read -e -p "Install add-ons ($ADDON_NAMES)?
[Y/n] " YN
if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
    echo -e "
<html>
<head>
<style>
  body {font-family: sans-serif;background:#ccc;}
  hr {margin-top: 1em;width:35%;}
  h1 {color:#f90;}
  h2 {color:#555;}
  img#logo {float:right;margin:1em;}
  img#id {width:25px;border:1px solid black;vertical-align:middle}
</style>
<title>Dotfiles: Install Mozilla addons for $(whoami)</title>
<link rel='shortcut icon' type='image/x-icon' href='http://mozilla.org/favicon.ico'></head>
  <body style='background:#ccc'>
  <img id='logo' src='https://mozilla.design/files/2019/10/Fx-Browser-icon-fullColor-512.png' />
  <h1>Hi, $(whoami)</h1>
  <h2>Firefox addon install links</h2>
  <ul>" > $PAGE
    echo -e $ADDONS >> $PAGE
    echo -e "</ul>
  <hr />
</body>
</html>" >> $PAGE && xdg-open $PAGE
fi

echo -e $SEP"Bye!"
