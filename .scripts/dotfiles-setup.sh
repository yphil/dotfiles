#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
shopt -s dotglob

DOTDIR=~/.dotfiles
LISPDIR=~/.emacs.d/elisp
SCRIPTDIR=~/.scripts
REPODIR=~/src
AUTOSTART_DIR=~/.config/autostart

SEP="\n################# "

BASICS="python zsh vim byobu apt-file curl wget htop bc locate git cowsay fortune fortunes-off zenity sox p7zip-full links unison baobab gparted xclip xsel smplayer xfce4-terminal traceroute openvpn xarchiver phototonic markdown fonts-noto"

declare -A CONF
CONF[sunvox_config.ini]="SunVox"
CONF[Qtractor.conf]="rncbc.org"
CONF[synthv1.conf]="rncbc.org"
CONF[Template.qtt]="rncbc.org"
CONF[config]="i3"
CONF[i3status.conf]="i3"
CONF[terminalrc]="xfce4/terminal"

# Various repos (that go in $REPODIR)
declare -A REPOS
REPOS[rupa/z]="github.com"
REPOS[yphil/pixilang-mode]="bitbucket.org"
REPOS[yphil/beatpicker]="bitbucket.org"
REPOS[yphil/kis]="bitbucket.org"
REPOS[yphil/weakpoint]="bitbucket.org"
REPOS[yphil/fwomaj]="bitbucket.org"

# git@bitbucket.org:yphil/beatpicker.git

declare -A PACKS
PACKS[dev_tools]="build-essential autoconf devscripts dpkg-dev-el default-jdk"
PACKS[beatnitpicker]="python-gst0.10 python-scipy python-matplotlib"
PACKS[optional]="nautilus-dropbox"
PACKS[image_tools]="gimp inkscape"
PACKS[music_prod]="qtractor qjackctl kxstudio-meta-audio-plugins-lv2 qmidinet calf-plugins hexter zam-plugins drumkv1-lv2 synthv1-lv2 samplv1-lv2 jalv lilv-utils guitarix artyfx fluid-soundfont-gm fluid-soundfont-gs zynaddsubfx helm audacious audacity vmpk cadence lv2-dev radium-compressor pizmidi-plugins oxefmsynth amsynth argotlunar yoshimi dpf-plugins qmidiarp rtirq-init distrho-plugin-ports-lv2 swh-lv2 triceratops-lv2 mda-lv2"
PACKS[games]="extremetuxracer supertuxkart chromium-bsu"
PACKS[emacs]="emacs aspell-fr"
PACKS[i3]="i3 dmenu i3status i3lock thunar scrot numlockx fonts-font-awesome"

# MOZilla addons (Largely outdated, but heck)
MOZURL="https://addons.mozilla.org/firefox/downloads/latest"
declare -A MOZADDONS
# MOZADDONS[Uppity]="$MOZURL/869/addon-869-latest.xpi"
MOZADDONS[Navigate_Up-WE]="$MOZURL/742705/addon-742705-latest.xpi"
# MOZADDONS[back_is_close]="$MOZURL/939/addon-939-latest.xpi"
MOZADDONS[Back_to_Close_WE]="$MOZURL/754960/addon-754960-latest.xpi"
MOZADDONS[GreaseMonkey]="$MOZURL/748/addon-748-latest.xpi"
MOZADDONS[ublock_origin]="$MOZURL/607454/addon-607454-latest.xpi"
MOZADDONS[Smart_Referer]="$MOZURL/327417/addon-327417-latest.xpi"
MOZADDONS[https_everywhere]="https://www.eff.org/https-everywhere"

echo -e $SEP"Dotfiles! #################

Welcome to Dotfiles, $(whoami) ; See README.md for documentation."

echo -e $SEP"Dotfiles and scripts"
read -e -p "#### Install / update dotfiles (in $DOTDIR) and scripts (in $SCRIPTDIR)? [Y/n] " YN
if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then

    cd $DOTDIR && echo "## $(pwd): " && git pull

    for i in .[^.#]* ; do
        if [[ ! -h ~/${i} && ${i} != ".git" ]] ; then
            [[ -e ~/${i} ]] && mv ~/${i} ~/${i}.orig
            ln -sv ${DOTDIR}/${i} ~/
        fi
    done
fi

echo -e $SEP"Various menial janitor tasks"

## declare an array variable
declare -a HOMEDIRS=(~/tmp ~/src ~/bin)

read -e -p "
#### Install basic packages ($BASICS) ? [Y/n] " YN

[[ $YN == "y" || $YN == "Y" || $YN == "" ]] && sudo apt install $BASICS

read -e -p "
#### Create base dirs, set (ZSH) shell, add user to audio? [Y/n] " YN

if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then

    for homedir in "${HOMEDIRS[@]}"
    do
        [[ ! -d ${homedir} ]] && mkdir -pv ${homedir} || echo -e "${homedir} \t\t\tOK"
    done
    [[ ! $SHELL == "/bin/zsh" ]] && chsh -s /bin/zsh || echo -e "ZSH shell \t\t\tOK"
    sudo adduser $(whoami) audio
fi

repos=$(printf "%s, " "${!REPOS[@]}")
read -e -p "
#### Install/Update git repos (${repos::-2})? [Y/n] " YN
if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
    for repo in "${!REPOS[@]}" ; do
        echo "## ${repo##*/}: "
        if [[ -d ${REPODIR}/${repo##*/} ]] ; then
            git -C ${REPODIR}/${repo##*/} pull
        else
            if [[ ${repo%%/*} == "yphil" ]] ; then
                git clone git@${REPOS[${repo}]}:${repo}.git ${REPODIR}/${repo##*/}
            else
                git clone https://${REPOS[${repo}]}/${repo}.git ${REPODIR}/${repo##*/}
            fi
        fi
    done
fi

confs=$(printf "%s, " "${!CONF[@]}")
read -e -p "
#### Symlink (${confs::-2}) config files? [Y/n] " YN
if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
    for conf in "${!CONF[@]}" ; do
        file="config/${CONF[$conf]}/$conf"
        echo -e "## ${CONF[$conf]}/$conf: "
        if [[ ! -d ~/.config/${CONF[$conf]} ]] ; then
            echo -e "Install ${CONF[$conf]}/$conf and run this script again".
        else
            if [[ ! -h ~/.$file ]] ; then
                ln -sfv ${DOTDIR}/$file ~/.config/${CONF[$conf]}/
            else
                echo "Already VC'd."
            fi
        fi
    done
fi

read -e -p "
#### Install package groups? [Y/n] " YN
if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
    for group in "${!PACKS[@]}" ; do
        read -e -p "
## Install $group? (${PACKS[$group]})
[Y/n] " YN
        [[ $YN == "y" || $YN == "Y" || $YN == "" ]] && sudo apt install ${PACKS[$group]}
    done
fi

PAGE=~/tmp/dotfiles-addons.html
ADDONS=""
ADDON_NAMES=""
echo -e $SEP"Mozilla add-ons"
for ADDON in "${!MOZADDONS[@]}" ; do
    ADDONS=$ADDONS"    <li><a href='"${MOZADDONS[$ADDON]}"'>$ADDON</a></li>\n"
    ADDON_NAMES=$ADDON", "$ADDON_NAMES
done
read -e -p "Install add-ons ($ADDON_NAMES)?
[Y/n] " YN
if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
    echo -e "
<html>
<head>
<style>
  body {font-family: sans-serif;background:#ccc;}
  hr {margin-top: 1em;width:35%;}
  h1 {color:#f90;}
  h2 {color:#555;}
  img#logo {float:right;margin:1em;}
  img#id {width:25px;border:1px solid black;vertical-align:middle}
</style>
<title>Dotfiles: Install Mozilla addons for $(whoami)</title>
<link rel='shortcut icon' type='image/x-icon' href='http://mozilla.org/favicon.ico'></head>
  <body style='background:#ccc'>
  <a href='http://opensimo.org/play/?a=Azer0,Counternatures' title='Music!'>
  <img id='logo' src='http://people.mozilla.com/~faaborg/files/shiretoko/firefoxIcon/firefox-128-noshadow.png' /></a>
  <h1>Hi $(whoami)</h1>
  <h2>click an addon to install it</h2>
  <ul>" > $PAGE
    echo -e $ADDONS >> $PAGE
    echo -e "</ul>
  <hr />
</body>
</html>" >> $PAGE && xdg-open $PAGE
fi

echo -e $SEP"Bye!"
