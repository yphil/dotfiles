#!/bin/env bash

EN=("🤩 Thanks, saved in my super cool #Free, #Libre and #OpenSource reader, put it in yours" "🤩 This goes *right* into my Free, Libre and #OpenSource #RSS reader, here's your instance" "Wow thanks, 🤩 another new cool feed in my #Free, #Libre and #OpenSource reader ; Here's yours" "BOOM 💣 new cool feed in my cool #Free, #Libre and #OpenSource #RSS reader ; Here's yours")
DE=("🤩 In meinem #Free, #Libre and #OpenSource RSS-Reader gespeichert" "🤩 Vielen Dank, ein neuer Feed in meinem coolen #Free, #Libre and #OpenSource RSS-Reader" "🤩 In meinem #Free, #Libre and #OpenSource RSS-Reader gespeichert")
ES=("🤩 Muchas gracias" "🤩 Muchas gracias, un nuevo feed en mi genial lector #Free, #Libre y #OpenSource" "🤩 Muchas gracias, un nuevo feed en mi genial lector #Free, #Libre y #OpenSource, aquí está el tuyo" "🤩 Salvado en el mío, aquí está el tuyo")
FR=("Merci !" "🤩 Merci beaucoup, un flux de plus dans mon super lecteur #RSS #Free, #Libre et #OpenSource que j'ai" "🤩 Merci beaucoup, un flux de plus dans mon super lecteur #RSS #Free, #Libre et #OpenSource, voilà le vôtre" "🤩 Sauvé dans la mienne, voilà la vôtre")

SIZE_EN=${#EN[@]} ; SIZE_DE=${#DE[@]} ; SIZE_ES=${#ES[@]}

if [[ $2 == "EN" ]] ; then
    CEILING=$SIZE_EN
elif [[ $2 == "DE" ]] ; then
    CEILING=$SIZE_DE
else
    CEILING=$SIZE_ES
fi

FLOOR=0 ; RANGE=$((($CEILING-1)-$FLOOR+1)) ; RESULT=$RANDOM
let "RESULT %= $RANGE";
RESULT=$(($RESULT+$FLOOR));

[[ ${1} == "--clipboard" ]] && URL=`xclip -selection c -o` || URL=$1

RES=`curl -sLI ${URL} | grep -i '^location:*' | tail -1 | cut -f2- -d' '`

[[ -z $RES ]] && RES=$URL

if [ `echo $RES | grep -o '/' | wc -l` -gt 2 ] ; then
    if [[ $RES == *"/feed"* ]] ; then
        URL=$(echo $RES | cut -f1-3 -d'/')    
    fi
fi 

echo "URL is $URL, and globbed it is ${URL##*/}"

PHRASE="$2[$RESULT]"

if [[ "$URL" =~ ^https://twitter.com.* ]]; then
		THANDLE=${URL##*/}
		RES="https://nitter.c-r-t.tk/$THANDLE/rss"
		PHRASE="Merci @$THANDLE ; je vous suis avec fidélité (dans un lecteur **libre**) merci pour votre travail 👍"
    echo "Yes, so now the url is $URL"
fi

notify-send $(echo -e "Searching: \n${RES}")


# Mandatory hack
TTT=`eval echo "$2[$RESULT] = ${!PHRASE}"`

# https://twitter.com/UPR_Asselineau
# https://www.youtube.com/@MLChristiansen

REQ=`~/.scripts/px-ptl-rss-link.js $RES`

if [[ $REQ == Error* ]] ; then
    notify-send -i help-hint "$REQ"
    paplay "/usr/share/sounds/freedesktop/stereo/dialog-error.oga"
else
    echo "${!PHRASE} https://petrolette.space/?add=$REQ" | xclip -f -in -selection clipboard
    notify-send -i help-hint "Feed URL (${REQ}) copied to clipboard"
    paplay "/usr/share/sounds/freedesktop/stereo/complete.oga"
fi
