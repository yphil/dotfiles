#!/bin/bash

echo_db_domains_browser=false
echo_git_domains_browser=false
echo_diffs=false
echo_db_not_git=false
echo_git_not_db=false
skip_git_clone=false
no_summary=false
github_links=false

usage() {
    echo "Usage: $0 [-a|--db_domains_browser] [-b|--git_domains_browser] [-c|--diffs] [-d|--db_not_git] [-e|--git_not_db] [-f|--skip_git_clone] [-g|--no_summary] [-h|--help] [-i|--github_links]" 1>&2; exit 1;
}

while getopts ":abcdefgi-:" option; do
    case $option in
        -)  # long args
            case "${OPTARG}" in
                db_domains_browser)
                    echo_db_domains_browser=true
                    ;;
                git_domains_browser)
                    echo_git_domains_browser=true
                    ;;
                diffs)
                    echo_diffs=true
                    ;;
                db_not_git)
                    echo_db_not_git=true
                    ;;
                git_not_db)
                    echo_git_not_db=true
                    ;;
                skip_git_clone)
                    skip_git_clone=true
                    ;;
                no_summary)
                    no_summary=true
                    ;;
                github_links)
                    github_links=true
                    ;;
                *)
                    if [ "$OPTERR" = 1 ] && [ "${optspec:0:1}" != ":" ]; then
                        echo "Unknown option --${OPTARG}" >&2
                        usage
                    fi
                    ;;
            esac
            ;;
        a)
            echo_db_domains_browser=true
            ;;
        b)
            echo_git_domains_browser=true
            ;;
        c)
            echo_diffs=true
            ;;
        d)
            echo_db_not_git=true
            ;;
        e)
            echo_git_not_db=true
            ;;
        f)
            skip_git_clone=true
            ;;
        g)
            no_summary=true
            ;;
        i)
            github_links=true
            ;;
        \?) # Invalid option
            usage
            exit 1
            ;;
    esac
done

DB_DIR_BROWSER="./scripts/browser"
DB_DIR_SERVER="./scripts/server"

GIT_URL="git@github.com:LINUSCULE/waplanner-domains.git"
CLONE_DIR="./repo"

if [[ "$skip_git_clone" == false ]]; then

    if [ -d "$CLONE_DIR" ]; then
        echo "Removing old clone..."
        rm -rf "$CLONE_DIR"
    fi

    echo "Cloning the repository..."
    git clone "$GIT_URL" "$CLONE_DIR"

fi

GIT_DIR_BROWSER="$CLONE_DIR/scripts/browser"
GIT_DIR_SERVER="$CLONE_DIR/scripts/server"

db_files_browser=0
git_files_browser=0
file_diffs_browser=0
only_bd_browser=0
only_git_browser=0

db_files_server=0
git_files_server=0
file_diffs_server=0
only_db_server=0
only_git_server=0

count_domains() {
    local dir=$1
    echo $(find "$dir" -mindepth 1 -maxdepth 1 -type d | wc -l)
}

list_domains() {
    local dir=$1
    local dir_list=""
    local dir_paths=$(find "$dir" -mindepth 1 -maxdepth 1 -type d)

    for dir_path in $dir_paths; do
        dir_list+=" - $(basename $dir_path)"
    done
        
    echo $dir_list
}

generate_github_url() {
  local input_path="$1"
  
  local relative_path="${input_path#./repo/}"

  local base_url="https://github.com/LINUSCULE/waplanner-domains/tree/main"
  local github_url="${base_url}/${relative_path}"

  if [[ "$github_links" == true ]]; then
      echo "$github_url"
  else
      echo "$input_path"
  fi

}

diff_files() {
    local db_dir=$1
    local git_dir=$2
    local type=$3

    local db_files_count=0
    local git_files_count=0
    local file_diffs_count=0
    local only_db_count=0
    local only_git_count=0

    # DB files
    while IFS= read -r -d '' db_file; do
        relative_path="${db_file#$db_dir}"
        git_file="$git_dir$relative_path"
        db_files_count=$((db_files_count + 1))

        if [ ! -f "$git_file" ]; then
            [[ $echo_db_not_git == true ]] && echo "$db_file is present in DB but not in Git."
            only_db_count=$((only_db_count + 1))
        else
            if ! diff --ignore-blank-lines --ignore-space-change --strip-trailing-cr "$db_file" "$git_file" > /dev/null; then
                [[ $echo_diffs == true ]] && echo "$db_file != $(generate_github_url $git_file)"
                file_diffs_count=$((file_diffs_count + 1))
            fi
        fi
    done < <(find "$db_dir" -type f -print0)

    # Git files
    while IFS= read -r -d '' git_file; do
        relative_path="${git_file#$git_dir}"
        db_file="$db_dir$relative_path"
        git_files_count=$((git_files_count + 1))

        if [ ! -f "$db_file" ]; then
            [[ $echo_git_not_db == true ]] && echo "$(generate_github_url $git_file) is present in Git but not in DB."
            only_git_count=$((only_git_count + 1))
        fi
    done < <(find "$git_dir" -type f -print0)

    if [ "$type" == "browser" ]; then
        db_files_browser=$db_files_count
        git_files_browser=$git_files_count
        file_diffs_browser=$file_diffs_count
        only_bd_browser=$only_db_count
        only_git_browser=$only_git_count
    elif [ "$type" == "server" ]; then
        db_files_server=$db_files_count
        git_files_server=$git_files_count
        file_diffs_server=$file_diffs_count
        only_db_server=$only_db_count
        only_git_server=$only_git_count
    fi
}

diff_files "$DB_DIR_BROWSER" "$GIT_DIR_BROWSER" "browser"
diff_files "$DB_DIR_SERVER" "$GIT_DIR_SERVER" "server"

db_num_domains_browser=$(count_domains "$DB_DIR_BROWSER")
db_num_domains_server=$(count_domains "$DB_DIR_SERVER")
git_num_domains_browser=$(count_domains "$GIT_DIR_BROWSER")
git_num_domains_server=$(count_domains "$GIT_DIR_SERVER")

db_domains_browser=$(list_domains "$DB_DIR_BROWSER")
db_domains_server=$(list_domains "$DB_DIR_SERVER")
git_domains_browser=$(list_domains "$GIT_DIR_BROWSER")
git_domains_server=$(list_domains "$GIT_DIR_SERVER")

if [[ "$echo_db_domains_browser" == true ]]; then
    echo ""
    echo "DB (BROWSER) DOMAINS"
    echo "==================="
    echo "db_domains_browser:" "$db_domains_browser"
fi

if [[ "$echo_git_domains_browser" == true ]]; then
    echo ""
    echo "GIT (BROWSER) DOMAINS"
    echo "==================="
    echo "git_domains_browser:" "$git_domains_browser"
fi

if [[ "$no_summary" == false ]]; then
    echo ""
    echo "BROWSER"
    echo "==================="
    echo "Domains (DB): $db_num_domains_browser"
    echo "Domains (Git): $git_num_domains_browser"
    echo "Scripts DB: $db_files_browser"
    echo "Scripts Git: $git_files_browser"
    echo "File diffs: $file_diffs_browser"
    echo "Files only present in DB: $only_bd_browser"
    echo "Files only present in Git: $only_git_browser"
    echo ""
    echo "SERVER"
    echo "==================="
    echo "Domains (DB): $db_num_domains_server"
    echo "Domains (Git): $git_num_domains_server"
    echo "Scripts DB: $db_files_server"
    echo "Scripts Git: $git_files_server"
    echo "File diffs: $file_diffs_server"
    echo "Files only present in DB: $only_db_server"
    echo "Files only present in Git: $only_git_server"
fi
