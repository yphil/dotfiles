#!/usr/bin/env bash

PHRASE=$(echo "$1" | wget -O- -i- | hxnormalize -x | hxselect -i "p.lead" | lynx -stdin -dump | sed '1d')

MOVE=$(echo $PHRASE | awk '{print $2}')
TIME=$(echo $PHRASE | awk '{print $9}')

[[ $MOVE = "basse" ]] && ICON="" || ICON=""

echo "$TIME $ICON" > /tmp/.next_tide_time.txt 
