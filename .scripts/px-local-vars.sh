#!/usr/bin/env bash

# cat ~/.local_variables.json
# {
#     "opensubtitles_api_key": "MyKey",
#     "opensubtitles_languages": "en,fr,ja",
#     "plage": "https://www.cabaigne.net/continent/country/city/horaire-marees.html"
# }

cat ~/.local_variables.json | jq -r .$1
