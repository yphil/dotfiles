#!/usr/bin/env bash

rm -f /tmp/.i3-kb-bindings.html
~/.scripts/px-i3-read-config.py -t html -o file
zenity --text-info --title='i3 Kb Shortcuts' --width=640 --height=800 --html --window-icon=info --filename=/tmp/.i3-kb-bindings.html
