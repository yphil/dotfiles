#!/usr/bin/env bash

# https://archive.org/download/quake-complete

GAMESDIR=~/bin/GAMES

GAMES=(
    stunt "Stunt Rally" $GAMESDIR/StuntRally-3.3-Linux/stuntrally3.sh
    flare "flare" flare
    pingus "pingus" pingus
    gpinball "Emilia" pinball
    doom doom "crispy-doom -iwad ~/src/iwad/doom.wad"
    $GAMESDIR/quake-16x16.png Quake "darkplaces -quake -basedir $GAMESDIR/quake-complete/Quake/"
    $GAMESDIR/quake-16x16.png "Quake: Scourge of Armagon" "darkplaces -hipnotic -basedir $GAMESDIR/quake-complete/Quake/"
    $GAMESDIR/quake-16x16.png "Quake: Dissolution of Eternity" "darkplaces -rogue -basedir $GAMESDIR/quake-complete/Quake/"
    $GAMESDIR/chromium-bsu-16x16.png "Chromium BSU" chromium-bsu
    astromenace "Astro Menace" $GAMESDIR/astromenace/astromenace
    powermanga PowerManga powermanga
    supertuxkart "Super Tux Kart" supertuxkart
    torcs "TORCS" torcs
    trigger-rally "Trigger Rally" trigger-rally
    opentyrian "Tyrian" opentyrian
    net.wz2100.warzone2100 "Warzone 2100" warzone2100
    applications-games CroMagRally $GAMESDIR/CroMagRally-3.0.1-linux-x86_64.AppImage
)

selected=$(yad \
               --list \
               --title="pX Games" \
               --window-icon=applications-games \
               --force-icon-size \
               --no-headers \
               --width=300 --height=500 \
               --print-column=3 \
               --separator=" " \
               --column=Icon:IMG \
               --column=Name \
               --column=Command:HD \
               "${GAMES[@]}" \
               --button="Run":0 \
               --button="Cancel":1)

echo $selected

[[ $? -eq 0 ]] && eval "$selected" || echo "No selection"
