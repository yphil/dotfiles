#!/bin/sh

for id in $(xinput --list|grep $1|perl -ne 'while (m/id=(\d+)/g){print "$1\n";}'); do
		setxkbmap -device $id -layout $2
    # echo "Device $1 ($id) set to $2"
done 
