#!/usr/bin/env python3

import os
import i3ipc
from html import escape
import ftfy

TMPFILE = os.path.expanduser("/tmp/.current_window_name.txt")

i3 = i3ipc.Connection()

thisId = 0

root = i3.get_tree()

def writeTmpFile(wname):

    
    if wname:
        wname = escape(wname)
    else:
        wname = 'Unnamed window'

    with open(TMPFILE, 'w', encoding='utf-8', errors='replace') as the_file:
        the_file.write(wname)

def on_window(i3, e):
    global thisId
    if e.change == "focus":
        thisId = e.container.id
        writeTmpFile(sanitize_utf8(e.container.name))
    if e.change == "title":
        if e.container.id == thisId:
            writeTmpFile(sanitize_utf8(e.container.name))

def on_workspace(i3, e):
    if e.change == "focus":
        writeTmpFile("Workspace " + e.current.name)

def sanitize_utf8(s):
    try:
        s = ftfy.fix_text(s)
    except Exception as e:
        print(f"Error fixing text: {e}")
    return s

i3.on('window', on_window)
i3.on('workspace', on_workspace)

i3.main()
