#!/usr/bin/env python3

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio, Gdk
import sys
import ast
import subprocess

list_of_args = sys.argv

# print(list_of_args[0])

lyrics = """Meet you downstairs in the bar and heard
your rolled up sleeves and your skull t-shirt
You say why did you do it with him today?
and sniff me out like I was Tanqueray

cause you're my fella, my guy
hand me your stella and fly
by the time I'm out the door
you tear men down like Roger Moore

I cheated myself
like I knew I would
I told ya, I was trouble
you know that I'm no good"""

class Window(Gtk.ApplicationWindow):

    def __init__(self, app):
        super(Window, self).__init__(title="pX info", application=app)

        style_provider = Gtk.CssProvider()

        css = b"""
        #meteo_label {
            border-bottom: 1px groove white;
        }
        """

        style_provider.load_from_data(css)
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

        grid = Gtk.Grid()

        menubar = Gtk.MenuBar()

        fmi = Gtk.MenuItem.new_with_label("File")

        menu = Gtk.Menu()
        emi = Gtk.MenuItem.new_with_label("Exit")
        emi.connect("activate", self.quitApp)
        menu.append(emi)

        fmi.set_submenu(menu)

        menubar.add(fmi)

        self.add(grid)

        meteo_doodle = subprocess.run(['curl', '-sS', 'https://wttr.in?0&T&Q'],
                                capture_output=True,
                                text=True).stdout

        meteo_label = Gtk.Label()
        meteo_label.set_markup('<tt>{}</tt>'.format(meteo_doodle))

        meteo_label.set_name("meteo_label")

        links_label = Gtk.Label()
        links_label.set_name("links_label")

        links_label.set_alignment(0.05,0)
        links_label.set_justify(Gtk.Justification.LEFT)
        links_label.set_markup("<a href='https://www.gtk.org' title='Our website'>GTK+ website</a>\n"
                               "<a href='https://www.gtk.org' title='Another website'>Other website</a>")

        grid.attach(menubar, 0, 0, 2, 1)
        grid.attach(meteo_label, 0, 1, 1, 1)
        grid.attach(links_label, 0, 2, 2, 1)

    def quitApp(self, par):

        app.quit()


class Application(Gtk.Application):

    def __init__(self):
        super(Application, self).__init__()

    def do_activate(self):

        self.win = Window(self)
        self.win.show_all()

    def do_startup(self):

        Gtk.Application.do_startup(self)

app = Application()
app.run()
