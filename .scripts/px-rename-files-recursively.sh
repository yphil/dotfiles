#!/usr/bin/env bash

shopt -s globstar nullglob

if [ "$#" -ne 2 ]; then
    echo "Usage: $(basename ${0}) pattern1-from pattern1-to"
else
    rename -v s/${1}/${2}/ **
fi
