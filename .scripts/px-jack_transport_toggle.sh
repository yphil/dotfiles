#!/bin/bash

function jack_transport_status() {
    jack_showtime | head -n1 & &> /dev/null
}

if jack_transport_status | grep -q -i stopped
then
    echo play | jack_transport &> /dev/null
    killall jack_showtime
else
    echo stop | jack_transport &> /dev/null
    killall jack_showtime
fi
