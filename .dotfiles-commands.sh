# This file is sourced from both my .zshrc and my .bashrc to ensure that I have the same env no matter what shell.

export PATH=:$PATH:~/.local/bin/:~/.scripts:~/bin

export ANDROID_HOME=~/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/platform-tools/
export PATH=$PATH:$ANDROID_HOME/cmdline-tools/latest/bin/
export PATH=$PATH:$ANDROID_HOME/emulator/
export PATH=$PATH:$ANDROID_HOME/build-tools/30.0.3/

export JAVA_HOME=~/bin/android-studio/jbr
export CAPACITOR_ANDROID_STUDIO_PATH=~/bin/android-studio/bin/studio.sh

export FLYCTL_INSTALL=~/.fly
export PATH=$FLYCTL_INSTALL/bin:$PATH

# LS_COLORS="fi=0:di=1;35:ln=31:mi=31:ex=35:*.py=1;32;41"
# export LS_COLORS="di=1;34:*.desktop=4;36"
export LS_COLORS='fi=0;37:di=1;34:ln=36:mi=31:or=35:so=32:pi=33:ex=32:bd=34;46:cd=34;43:*.mp3=0;37'

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"

if [ -n "$DESKTOP_SESSION" ];then
    eval $(gnome-keyring-daemon --start)
    export SSH_AUTH_SOCK
fi

alias aa="sudo apt install"
alias aaa="sudo apt autoremove -y && sudo apt upgrade && ~/.scripts/px-i3-upgradeable-packages.sh"
alias aaaa="sudo apt update && sudo apt upgrade && sudo apt autoremove -y && sudo apt dist-upgrade && ~/.scripts/px-i3-upgradeable-packages.sh"
alias af="apt-file search"
alias al="dpkg -L"
alias ap="apt-cache policy"
alias ar="sudo apt purge"
alias as="apt show -a"
alias au="sudo apt update"
alias c="cat"
alias d="cd ~/.dotfiles && git status"
alias e="px-emacs.sh"
alias gc="git commit -a -m"
alias gd="git diff"
alias gh="ssh-add ~/.ssh/id_rsa.github.com"
alias gl="git log --pretty=format:'%Cblue%h%Creset%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)%an%Creset' --abbrev-commit --date=iso"
alias gp="git push origin"
alias gs="git status && git show --name-status"
alias grep="grep --exclude-dir='.git'"
alias l="locate -i"
alias ll="ls -lha"
alias lls="du -h --max-depth=0 * | sort -h"
alias llt="ls -ltr"
alias ls="ls --color"
alias pp='ps aux | ag $(echo $1 | sed "s/^\(.\)/[\1]/g")'
alias rr="rehash"
alias tt="sudo tcpdump"
alias rm="rm -i"
alias ww="which"

if [ -n "$ZSH_VERSION" ]; then
    alias -g s="~/src"
    alias -g t="~/tmp"
else
    alias s="cd ~/src/"
    alias t="cd ~/tmp"
    alias src="cd ~/src"
fi

alias xcopy="xclip -selection c"
alias xpaste="xclip -selection c -o"

HEADER="\n####"

function px-list-git-files() {
    git ls-files | while read file; do
        echo "    './$file',"
    done
}

function a() {
    if [ -z "$2" ] ; then
        apt-cache search $1
    else
        apt-cache search $1 | grep $2
    fi
}

function fontello() {
    rm -rfv fontello-*
    unzip fontello.zip && cp -fv fontello-*/config.json font/fontello-config.json && cp -fv fontello-*/css/*.css css/ && cp -fv fontello-*/font/* font/
}

function hh () {
    OUTPUT=$(grep $1 $HISTFILE | grep $2 | cut -c3- | sed "s/:0;/ /")
    # TIMESTAMP=${OUTPUT%%:0*}
    # COMMAND =
    # echo $TIMESTAMP
    # echo $OUTPUT

    for LINE in $OUTPUT ; do
        TIMESTAMP=$(echo $LINE | cut -d' ' -f 1)
        CMD=$(echo $LINE | cut -d' ' -f 1 --complement)
        # echo $LINE | cut -d' ' -f 1
        echo $CMD
    done
    # echo "Command: $CMD TS: $TIMESTAMP"

}

function px-md () {
    pandoc $1 | lynx -stdin
}

function color() {
  local color=$1 style=$2 b=0

  shift

  case $style in
    bold|b)           b=1; shift ;;
    italic|i)         b=2; shift ;;
    underline|u)      b=4; shift ;;
    inverse|in)       b=7; shift ;;
    strikethrough|s)  b=9; shift ;;
  esac

  case $color in
    black|b)    echo "\033[${b};30m${@}\033[0;m" ;;
    red|r)      echo "\033[${b};31m${@}\033[0;m" ;;
    green|g)    echo "\033[${b};32m${@}\033[0;m" ;;
    yellow|y)   echo "\033[${b};33m${@}\033[0;m" ;;
    blue|bl)    echo "\033[${b};34m${@}\033[0;m" ;;
    magenta|m)  echo "\033[${b};35m${@}\033[0;m" ;;
    cyan|c)     echo "\033[${b};36m${@}\033[0;m" ;;
    white|w)    echo "\033[${b};37m${@}\033[0;m" ;;
    *)          echo "\033[${b};38;5;$(( ${color} ))m${@}\033[0;m" ;;
  esac
}

function px-pull-in-space () {
    echo -e "${HEADER} Pulling on server"
    ssh -t sbc 'cd ~/www/petrolette && git stash && git pull && rm -rfv node_modules/feedrat/ && rm -rfv node_modules/favrat/ && npm install --no-optional && ./node_modules/pm2/bin/pm2 list'
    echo -e "${HEADER} Pulled on server"
}

function px-push-branch-to-master () {

    BRANCH=$(git symbolic-ref --short HEAD)
    DEPLOY=false
    RELEASE=false
    JUSTMERGE=false
    COMMIT_MSG="No commit MSG"
    VALID_ARGS=false
    VALID_NUM_ARGS=false
    VERSION_NUM=$(jq -r '.version' ./package.json)

    if [ "$#" -ne 2 ] ; then
        VALID_NUM_ARGS=false
    else
        VALID_NUM_ARGS=true
    fi

    if [ "${1}" = "--deploy" ] || [ "${1}" = "-d" ] ; then
        DEPLOY=true
        RELEASE=true
        VALID_ARGS=true
        COMMIT_MSG="${2}"
    elif [ "${1}" = "--release" ] || [ "${1}" = "-r" ]; then
        DEPLOY=false
        RELEASE=true
        VALID_ARGS=true
        COMMIT_MSG="${2}"
    elif [ "${1}" = "--just-merge" ] || [ "${1}" = "-j" ]; then
        JUSTMERGE=true
        VALID_ARGS=true
        COMMIT_MSG="${2}"
    fi

    if ! [[ $VALID_ARGS == true &&  $VALID_NUM_ARGS == true ]] ; then
        echo "Usage: ${0} --deploy / -d, --release / -r, --just-merge / -j  'Commit message / Version number' \n"
        exit 1
    fi

    if [[ ${BRANCH} =~ ^(protected|master)$ ]]; then
        echo -e "${HEADER} Not in a sub-branch"
    else

        git commit -am ${COMMIT_MSG}

        if ${RELEASE} ; then

            npm version patch

            git tag -a $(jq -r '.version' ./package.json) -m ${COMMIT_MSG}

            if hash auto-changelog 2>/dev/null; then
                auto-changelog -p --hide-credit --template keepachangelog
            fi

        git push --tags origin HEAD

        git checkout master
        git pull origin master
        git merge --no-edit --no-ff ${BRANCH} -m ${COMMIT_MSG}
        git push origin master

        git checkout ${BRANCH}

        if ${DEPLOY} ; then
            px-pull-in-space
        fi
    fi
        echo -e "${HEADER} Done"
    fi
}

px-disable-screensaver () {
    xset s 0 0
    xset s off
    # exit 0
}

px-syslog () {
    if [[ ${#} -eq 0 ]] ; then
        sudo tail -f -n 40 /var/log/syslog
    else
        sudo cat /var/log/syslog | grep -i --color=auto ${1}
    fi
}

zz () {
    zz=~/tmp/zz
    [[ ! -d ${zz} ]] && mkdir -pv ${zz}
    cd ${zz} && rm -rf ${zz}/*
}

px-install-update-sunvox() {
    s=~/bin/Sunvox
    [[ ! -d ${s} ]] && mkdir -pv ${s}
    curl -o "${s}/sunvox.zip" -LOk --progress-bar http://www.warmplace.ru/soft/sunvox/sunvox.zip && unzip -oq "${s}/sunvox.zip" -d ${s}
    if [[ ! -e ~/bin/sunvox && -d ${s}/sunvox ]] ; then
        [[ $(uname -m) == "x86_64" ]] && ln -sv ${s}/sunvox/sunvox/linux_x86_64/sunvox ~/bin/ || ln -sv ${s}/sunvox/sunvox/linux_x86/sunvox ~/bin/
    fi

    echo -e "
### Successfully installed Sunvox version $(head -1 ${s}/sunvox/docs/changelog.txt | sed 's/.$//')"
}

jack_transport_status() {
    jack_showtime | head -n1 & &> /dev/null
}

px-jack_toggle_play() {
    if jack_showtime | head -n1 | grep -q -i stopped
    then
        echo play | jack_transport
        killall jack_showtime
    else
        echo stop | jack_transport
        killall jack_showtime
    fi
}

px-video-30fps () {
    ffmpeg -i $1 -r 30 $1-30fps.mkv
}

px-video-trim () {
    ffmpeg -i $1 -ss $2 -c copy -to $3 $1-trimmed.mp4
}

px-video-scast () {
    ffmpeg -f alsa -ac 2 -i pulse -f x11grab -r 30 -s 1882x1200 -i :0.0+38,0 -acodec pcm_s16le -vcodec libx264 -preset ultrafast -threads 0 $1.mkv
}

px-video-resize () {
ffmpeg \
    -i "$1" \
    -map 0 \
    -vf "scale=iw*sar*min($MAX_WIDTH/(iw*sar)\,$MAX_HEIGHT/ih):ih*min($MAX_WIDTH/(iw*sar)\,$MAX_HEIGHT/ih),pad=$MAX_WIDTH:$MAX_HEIGHT:(ow-iw)/2:(oh-ih)/2" \
    $1.mkv
}

px-bell () {
    [[ $# -eq 0 ]] && SNDFILE="complete" || SNDFILE="dialog-error"
    paplay "/usr/share/sounds/freedesktop/stereo/${SNDFILE}.oga"
}

px-search-and-replace-here () {
    find ./ -type f -exec sed -i -e "s/$1/$2/g" {} \;
}

px-cleanup-filenames () {
    find -type f | rename -v 's/%20/_/g'
    find -type f | rename -v 's/ /_/g'
    find -type f | rename -v 's/_-_/-/g'
}

px-reorder-filenames () {

    digits=%04d.%s

    mkdir -vp tmp && rm -rf tmp/*

    X=1;
    for i in *; do
        newfile=$(printf ${digits} ${X%.*} ${i##*.})
        color="\e[32m"

        if [ -f "$i" ] ; then
            [[ "$i" != "$newfile" ]] && color="\e[31m"
            cp $i tmp/$(printf ${digits} ${X%.*} ${i##*.}) && echo "$color$i => $(printf %04d.%s ${X%.*} ${i##*.})\e[0m"
        fi

        let X="$X+1"
    done

    find . -maxdepth 1 -type f -exec rm -rf '{}' \; && cp tmp/* . && rm -rf tmp/ && echo "Processed $X files"
}

px-broadcast-mic-to-ip () {
    arecord -f dat | ssh -C $1 aplay -f dat
}

px-guitar-tuner () {
    for N in E2 A2 D3 G3 B3 E4;do play -n synth 4 pluck $N repeat 2;done
}

md () {
    mkdir -p ${1} && cd ${1}
}

px-wake-up-trackpad () {
    sudo rmmod psmouse
    sudo modprobe psmouse
}

px-dirsizes () { for DIR in $1* ; do if [ -d $DIR ] ; then du -hsL $DIR ; fi ; done }

px-find-this-and-do-that () {
    find . -iname ${1} -exec ${2} '{}' \;
}

px-bkp () {
    cp -Rp $1 ${1%.*}.bkp-$(date +%y-%m-%d-%Hh%M).${1#*.}
}

px-ip () {
    echo -e "Local:   $(ip -o -4 addr show | awk -F '[ /]+' '/global/ {print $4}')"
    echo -e "distant: $(dig +short myip.opendns.com @resolver1.opendns.com)"
}

drawProgressBar() {
    local duration=$1
    local interval=1
    local progress=0

    while [ $progress -lt $duration ]; do
        local percentage=$((progress * 100 / duration))
        local completed=$((progress * 50 / duration))
        local remaining=$((50 - completed))

        printf -v progressBar "[%s=%*s] %d%% (%ds)" "$(printf '=%.0s' $(seq 1 $completed))" $remaining '' $percentage $((duration - progress))
        echo -ne "$progressBar\r"

        sleep $interval
        ((progress += interval))
    done
    echo -e "\n"
}

px-remind-me-this-in () {

    if [ $# -lt 2 ]; then
        echo "Usage: $0 <text> <duration_in_minutes>"
        exit 1
    fi

    duration=$2

    drawProgressBar $((duration * 60))
    px-bell
    zenity --info --text=$1
    notify-send -i help-hint $1
}

px-notes () {
    if [ ! $1 ] ; then
echo -e "
################# NOTES
Session=lightdm-xsession
Aventurer les hypotheses
nabil.zakhbat@gmail.com
Qtractor menu : Shift-F12
/ssh:user@machine:
git reset --hard HEAD@{7}
ZSH : rm -rf ^survivorfile
BASH: rm -f !(survivor_file)
0608853025
find . -type f -printf '%TY-%Tm-%Td %TT %p
' | sort
last arg of last command : !$
zdump Africa/Morocco Europe/Paris
tar -tf <file.tar.gz> | xargs rm -r
gnome-terminal --command byobu --maximize --hide-menubar
ESC DOT pops the last argument of the last command
DNS1 212.217.1.1 DNS2 .12 p.nom PPPoE / LLC
grep . * to cat a bunch of (small) files
ssh machine -L127.0.0.1:3306:127.0.0.1:3306
middleman build --clean && git commit -a -m 'new local build OK' && git push origin master
a && middleman build --clean && Commit 'deployed' && Push master
if ('$term' == emacs) set term=dumb
sudo ln -s /usr/lib/i386-linux-gnu/libao.so.4 /usr/lib/libao.so.2
sshfs name@server:/path/to/folder /path/to/mount/point

# reset the index to the desired tree
git reset 56e05fced
# move the branch pointer back to the previous HEAD
git reset --soft HEAD@{1}
git commit -m 'Revert to 56e05fced'
# Update working copy to reflect the new commit
git reset --hard
## Use px-notes \"this is a new note\" to add a note
"
else
        sed -i '/^################# NOTES/a '$1'' ~/.dotfiles/.dotfiles-commands.sh && k && Commit "New note : $1" && Push master && cd -
fi
}

[[ -e ~/src/z/z.sh ]] && . ~/src/z/z.sh
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

# Init
zdump Africa/Morocco Europe/Paris

if (type "cowsay" > /dev/null && type "fortune" > /dev/null ); then
    cowsay `fortune -a` ; tput setaf 2 ; echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" ; tput sgr0
else
    [[ -x "${HOME}/.scripts/px-cowsay.pl" ]] && perl ${HOME}/.scripts/px-cowsay.pl fortune
fi

echo "${0} loaded OK"
