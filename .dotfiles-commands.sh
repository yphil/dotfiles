# This file is sourced from both my .zshrc and my .bashrc to ensure that I have the same env no matter what shell.
# See https://bitbucket.org/yphil/dotfiles

PATH=~/.scripts:~/bin:$PATH
export LESS_TERMCAP_so=$'\E[30;43m'
export PATH="/home/px/.cask/bin:/home/px/.nvm/versions/node/v8.5.0/bin/:$PATH"

alias Commit="git commit -am"
alias L="locate -i"
alias Push="git push origin"
alias a="sudo apt install"
alias aa="apt-cache search"
alias af="apt-file search"
alias ag="ag --path-to-agignore ~/.ignore"
alias al="dpkg -L"
alias ap="apt-cache policy"
alias ar="sudo apt purge"
alias as="apt show"
alias c="cat"
alias cp="cp -i"
alias grep="grep --exclude-dir='.git'"
alias l="less"
alias ll="ls -lha"
alias lls="du -h --max-depth=0 * | sort -h"
alias llt="ls -ltr"
alias ls="ls --color"
alias pss='ps aux | grep $(echo $1 | sed "s/^\(.\)/[\1]/g")'
alias rm="rm -i"
alias s="cd ~/src"
alias t="cd ~/tmp"
alias v="gpicview"

HEADER="\n####"

function color() {
  local color=$1 style=$2 b=0

  shift

  case $style in
    bold|b)           b=1; shift ;;
    italic|i)         b=2; shift ;;
    underline|u)      b=4; shift ;;
    inverse|in)       b=7; shift ;;
    strikethrough|s)  b=9; shift ;;
  esac

  case $color in
    black|b)    echo "\033[${b};30m${@}\033[0;m" ;;
    red|r)      echo "\033[${b};31m${@}\033[0;m" ;;
    green|g)    echo "\033[${b};32m${@}\033[0;m" ;;
    yellow|y)   echo "\033[${b};33m${@}\033[0;m" ;;
    blue|bl)    echo "\033[${b};34m${@}\033[0;m" ;;
    magenta|m)  echo "\033[${b};35m${@}\033[0;m" ;;
    cyan|c)     echo "\033[${b};36m${@}\033[0;m" ;;
    white|w)    echo "\033[${b};37m${@}\033[0;m" ;;
    *)          echo "\033[${b};38;5;$(( ${color} ))m${@}\033[0;m" ;;
  esac

}


function px-get-path-permissions () {

    HERE=$(pwd)
    for i in $(seq 0 $(pwd | tr -cd / | wc -c)) ; do pwd && ls -lad && cd .. ; done
    cd ${HERE}

}

function px-pull-in-space () {
    echo -e "${HEADER} Pulling on server"
    ssh -t pc 'cd ~/www/petrolette && git stash && git pull && npm install && ./node_modules/pm2/bin/pm2 show ptl'
    echo -e "${HEADER} Pulled on server"
}

function px-test () {
    echo -e "${HEADER}"
}

function px-push-branch-to-master () {

    BRANCH=$(git symbolic-ref --short HEAD)
    DEPLOY=false
    RELEASE=false

    if [[ "${1}" == "--deploy" ]] ; then
        DEPLOY=true
        COMMIT_MSG="${2}"
    elif [[ "${1}" == "--release" ]]; then
        DEPLOY=true
        RELEASE=true
        COMMIT_MSG="Version %s \"${2}\" - $(date '+%d/%m/%Y %H:%M:%S')"
    else
        COMMIT_MSG="${1}"
    fi

    # echo -e "\n1: ${1} 2: ${2} COMMIT_MSG: ${COMMIT_MSG} Deploy: ${DEPLOY} Release: ${RELEASE} Branch: (${BRANCH})"

    if [[ ${BRANCH} =~ ^(protected|master)$ ]]; then
        echo -e "${HEADER} Not in a sub-branch"
    else
        MSG="Pushed to master "
        git commit -am ${COMMIT_MSG}

        if ${RELEASE} ; then
            MSG="${MSG}& Released "
            npm version patch -m "${COMMIT_MSG}"
        fi

        git push --tags origin HEAD

        git checkout master
        git pull origin master
        git merge --no-edit --no-ff ${BRANCH}
        git push origin master

        git checkout ${BRANCH}

        if ${DEPLOY} ; then
            px-pull-in-space
            MSG="${MSG}& Deployed "
        fi
            echo -e "${MSG}[${BRANCH}] (${COMMIT_MSG})"
    fi
}

px-image-tag-viewer () {
    TAGS=
    SLIDESHOW_OPS=
    for i in ${*}; do
        [[ ${i} =~ '^[0-9]+$' ]] && SLIDESHOW_OPS="-D${i} --fullscreen" || TAGS+="${i}\|"
    done

    exiftool -ext .JPG -fast -p '$directory/$filename;$Keywords' -qq -r -m . 2> /dev/null | grep -i "\;.*${TAGS: : -2}" | sed 's/\;.*//' > /tmp/imglist.txt && feh ${SLIDESHOW_OPS} --auto-zoom --filelist /tmp/imglist.txt
    rm -fv /tmp/imglist.txt
}

px-disable-screensaver () {
    xset s 0 0
    xset s off
    # exit 0
}

px-syslog () {
    if [[ ${#} -eq 0 ]] ; then
        sudo tail -f -n 40 /var/log/syslog
    else
        sudo cat /var/log/syslog | grep -i --color=auto ${1}
    fi
}

zz () {
    zz=~/tmp/zz
    [[ ! -d ${zz} ]] && mkdir -pv ${zz}
    cd ${zz} && rm -rf ${zz}/*
}

px-install-update-sunvox() {
    s=~/bin/Sunvox
    [[ ! -d ${s} ]] && mkdir -pv ${s}
    curl -o "${s}/sunvox.zip" -LOk --progress-bar http://www.warmplace.ru/soft/sunvox/sunvox.zip && unzip -oq "${s}/sunvox.zip" -d ${s}
    if [[ ! -e ~/bin/sunvox && -d ${s}/sunvox ]] ; then
        [[ $(uname -m) == "x86_64" ]] && ln -sv ${s}/sunvox/sunvox/linux_x86_64/sunvox ~/bin/ || ln -sv ${s}/sunvox/sunvox/linux_x86/sunvox ~/bin/
    fi

    echo -e "
### Successfully installed Sunvox version $(head -1 ${s}/sunvox/docs/changelog.txt | sed 's/.$//')"
}

px-vpn() {
    echo -e "\nLaunching VPN"
    sudo openvpn --config ~/.ssh/client_out.conf
    echo -e "\nNow sleeping for 15 seconds"
    sleep 15
    sudo route del default gw 192.168.1.1
    sudo route add default gw 10.9.0.1
    sudo route add -net 192.168.1.0/24 gw 192.168.1.1
    sudo route add -host 213.246.42.252 gw 192.168.1.1
    echo -e "\nVPN OK"
}

px-vpn-routing() {
    sudo route del default gw 192.168.1.1
    sudo route add default gw 10.9.0.1
    sudo route add -net 192.168.1.0/24 gw 192.168.1.1
    sudo route add -host 213.246.42.252 gw 192.168.1.1
    echo -e "\nVPN OK"
}

jack_transport_status() {
    jack_showtime | head -n1 & &> /dev/null
}

px-jack_toggle_play() {
    if jack_showtime | head -n1 | grep -q -i stopped
    then
        echo play | jack_transport
        killall jack_showtime
    else
        echo stop | jack_transport
        killall jack_showtime
    fi

}

px-freemem () {
    item=$(egrep 'MemAvailable' /proc/meminfo | awk '{print $2;}')
    total=$(egrep 'MemTotal' /proc/meminfo | awk '{print $2;}')
    percent=$(awk "BEGIN { pc=100*${item}/${total}; i=int(pc); print (pc-i<0.5)?i:i+1 }")
    echo "$percent%"
}

px-qtractor-takes-cleanup () {
    [[ "${2:-}" == "--delete" ]] && COMMAND="rm -fv" || COMMAND="ls"
    echo "Usage: $0 qtr_session_file [--delete] \n"
    for file in *.wav* *.mid* ; do
        grep -q -F "$file" $1 || eval $COMMAND " $file"
    done
}

px-video-30fps () {
    ffmpeg -i $1 -r 30 $1-30fps.mkv
}

px-video-trim () {
    # [[ $# -ne 3 ]] && echo "Usage: $0 file start end" ||
    ffmpeg -i $1 -ss $2 -c copy -to $3 $1-trimmed.mp4
    # ffmpeg -i $1 -ss 00:00:00 -c copy -t $3 $1-trimmed.mp4
}

px-video-30fps-no_sound () {
    ffmpeg -i $1 -r 30 -an $1.mkv
}

px-video-scast () {
    ffmpeg -f alsa -ac 2 -i pulse -f x11grab -r 30 -s 1882x1200 -i :0.0+38,0 -acodec pcm_s16le -vcodec libx264 -preset ultrafast -threads 0 $1.mkv
}

px-video-resize () {
ffmpeg \
    -i "$1" \
    -map 0 \
    -vf "scale=iw*sar*min($MAX_WIDTH/(iw*sar)\,$MAX_HEIGHT/ih):ih*min($MAX_WIDTH/(iw*sar)\,$MAX_HEIGHT/ih),pad=$MAX_WIDTH:$MAX_HEIGHT:(ow-iw)/2:(oh-ih)/2" \
    $1.mkv
}

px-bell () {
    [[ $# -eq 0 ]] && MSG="Heads UP!" || MSG="$1"
    paplay /usr/share/sounds/freedesktop/stereo/complete.oga
    notify-send -i help-hint ${MSG}
}

px-search-and-replace-here () {
    find ./ -type f -exec sed -i -e "s/$1/$2/g" {} \;
}

px-kx-repos-install () {
    sudo apt-get install apt-transport-https software-properties-common wget
    wget https://launchpad.net/~kxstudio-debian/+archive/kxstudio/+files/kxstudio-repos_9.4.1~kxstudio1_all.deb
    sudo dpkg -i kxstudio-repos_9.4.1~kxstudio1_all.deb
    sudo apt-get install libglibmm-2.4-1v5
    wget https://launchpad.net/~kxstudio-debian/+archive/kxstudio/+files/kxstudio-repos-gcc5_9.4.1~kxstudio1_all.deb
    sudo dpkg -i kxstudio-repos-gcc5_9.4.1~kxstudio1_all.deb
}

px-cleanup-filenames () {
    find -type f | rename -v 's/%20/_/g'
    find -type f | rename -v 's/ /_/g'
    find -type f | rename -v 's/_-_/-/g'
}

px-reorder-filenames () {

    digits=%04d.%s

    mkdir -vp tmp && rm -rf tmp/*

    X=1;
    for i in *; do
        newfile=$(printf ${digits} ${X%.*} ${i##*.})
        color="\e[32m"

        if [ -f "$i" ] ; then
            [[ "$i" != "$newfile" ]] && color="\e[31m"
            cp $i tmp/$(printf ${digits} ${X%.*} ${i##*.}) && echo "$color$i => $(printf %04d.%s ${X%.*} ${i##*.})\e[0m"
        fi

        let X="$X+1"
    done

    find . -maxdepth 1 -type f -exec rm -rf '{}' \; && cp tmp/* . && rm -rf tmp/ && echo "Processed $X files"
}

ssh () {
    if [ $# -eq 1 ] ; then
        tmux rename-window `echo $1 | sed 's/.*@//g' | sed 's/.local//g'`
    fi
    command ssh $*
}

px-git-last-commit-to-clipboard () {
    git log | head -1 | cut -c 8-47 | xclip -selection clipboard
    echo "Last commit ($(git log | head -3 | cut -c 9-31 | tail -1) - $(git log | head -5 | cut -c 5-47 | tail -1)) copied to clipboard"
}

px-broadcast-mic-to-ip () {
    arecord -f dat | ssh -C $1 aplay -f dat
}

px-guitar-tuner () {
    for N in E2 A2 D3 G3 B3 E4;do play -n synth 4 pluck $N repeat 2;done
}

md () {
    mkdir -p ${1} && cd ${1}
}

px-vnc () {
    \ssh -f -L 5900:127.0.0.1:5900 $1 "x11vnc -scrollcopyrect -noxdamage -localhost -nopw -once -display :0" ; vinagre 127.0.0.1:5900
}

px-lan-scan () {
    # nmap -sP 192.168.1.0/24
    LOCAL_MASK=$(ip -o -4 addr show | awk -F '[ /]+' '/global/ {print $4}' | cut -d. -f1,2,3)
    GATEWAY=$(route -n | \grep '^0.0.0.0' | awk '{print $2}')
    [[ $# -eq 0 ]] && range="10" || range=$1

    for num in $(seq 1 ${range}) ; do
        IP=$LOCAL_MASK.$num
        [[ $IP == $GATEWAY ]] && MACHINE="gateway" || MACHINE=$(avahi-resolve-address $IP 2>/dev/null | sed -e :a -e "s/$IP//g;s/\.[^>]*$//g;s/^[ \t]*//")
        ping -c 1 $IP>/dev/null
        [[ $? -eq 0 ]] && echo -e "UP    $IP \t ($MACHINE)" || echo -e "DOWN  $IP"
    done
}

px-wake-up-trackpad () {
    sudo rmmod psmouse
    sudo modprobe psmouse
}

px-dirsizes () { for DIR in $1* ; do if [ -d $DIR ] ; then du -hsL $DIR ; fi ; done }

px-find-this-and-do-that () {
    find . -iname ${1} -exec ${2} '{}' \;
}

px-bkp () {
    cp -Rp $1 ${1%.*}.bkp-$(date +%y-%m-%d-%Hh%M).${1#*.}
}

px-ip () {
    echo -e "Local:   $(ip -o -4 addr show | awk -F '[ /]+' '/global/ {print $4}')"
    echo -e "distant: $(dig +short myip.opendns.com @resolver1.opendns.com)"
}

px-remind-me-this-in () {
    sleep $2
    zenity --info --text=$1
}

px-netstats () {
    if hash ss 2>/dev/null; then
        echo -e "      $(ss -p | cut -f2 -sd\" | sort | uniq | wc -l) processes : $(ss -p | cut -f2 -sd\" | sort | uniq | xargs) \n"
    fi
    lsof -P -i -n | uniq -c -w 10
    echo -e "
Distant connected IPs : \n $(netstat -an | grep ESTABLISHED | awk '{print $5}' | awk -F: '{print $1}' | sort | uniq -c | awk '{ printf("%s\t%s\t",$2,$1) ; for (i = 0; i < $1; i++) {printf("*")}; print "" }') \n"
    if [ $1 ] ; then
        netstat -luntp
        echo -e "
Connected hostnames"
        for IP in $(netstat -an | grep ESTABLISHED | awk '{print $5}' | awk -F: '{print $1}' | sort | uniq); do host ${IP} | sed 's/\.in-addr.arpa domain name pointer/ \=\> /' ; done | grep -v '^;'
    else
        echo "use -a to see machine names (slow)"
    fi
}

px-notes () {
    if [ ! $1 ] ; then
echo -e "
################# NOTES
<<<<<<< HEAD
<<<<<<< HEAD
nabil.zakhbat@gmail.com
unison -fat -fastcheck true -batch ~/tmp/.pr0n /media/px/Nokia N9/tmp/.pr0n/
=======
>>>>>>> 4fb34eb0a527a22ea9417c56514b6e1c696e30ff
=======
Qtractor menu : Shift-F12
>>>>>>> 06bbe6bea2d89c606a1ffcec28ba975d6892a83f
/ssh:user@machine:
MAC Address: 48:A2:2D:E1:79:74 (Shenzhen Huaxuchang Telecom Technology Co.)
MAC Address: 48:A2:2D:E1:79:74 (Shenzhen Huaxuchang Telecom Technology Co.)
git reset --hard HEAD@{7}
ZSH : rm -rf ^survivorfile
BASH: rm -f !(survivor_file)
0608853025
find . -type f -printf '%TY-%Tm-%Td %TT %p
' | sort
last arg of last command : !$
zdump Africa/Morocco Europe/Paris
tar -tf <file.tar.gz> | xargs rm -r
gnome-terminal --command byobu --maximize --hide-menubar
ESC DOT pops the last argument of the last command
DNS1 212.217.1.1 DNS2 .12 p.nom PPPoE / LLC
grep . * to cat a bunch of (small) files
ssh machine -L127.0.0.1:3306:127.0.0.1:3306
middleman build --clean && git commit -a -m 'new local build OK' && git push origin master
a && middleman build --clean && Commit 'deployed' && Push master
if ('$term' == emacs) set term=dumb
sudo ln -s /usr/lib/i386-linux-gnu/libao.so.4 /usr/lib/libao.so.2
sshfs name@server:/path/to/folder /path/to/mount/point

# reset the index to the desired tree
git reset 56e05fced
# move the branch pointer back to the previous HEAD
git reset --soft HEAD@{1}
git commit -m 'Revert to 56e05fced'
# Update working copy to reflect the new commit
git reset --hard

## Use px-notes \"this is a new note\" to add a note
"
else
        sed -i '/^################# NOTES/a '$1'' ~/.dotfiles/.dotfiles-commands.sh && k && Commit "New note : $1" && Push master && cd -
fi
}

# z (https://github.com/rupa/z)
[[ -e ~/src/z/z.sh ]] && . ~/src/z/z.sh

# me=`basename "$0"`

echo "${0} loaded OK"
